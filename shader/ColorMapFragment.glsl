
varying lowp vec2 TexCoordOut;

uniform sampler2D Texture;
uniform sampler2D Texture2;

uniform lowp float alpha;

void main(void) {
        
    lowp vec4 textureColor = texture2D(Texture, TexCoordOut);
    
    lowp vec4 textureColorR = texture2D(Texture2, vec2(textureColor.r, 0.166));
    lowp vec4 textureColorG = texture2D(Texture2, vec2(textureColor.g, 0.5));
    lowp vec4 textureColorB = texture2D(Texture2, vec2(textureColor.b, 0.833));
    
    lowp vec4 mapColor = vec4(textureColorR.r, textureColorG.g, textureColorB.b, textureColor.w );
    
    mapColor = mapColor * alpha;
    
    gl_FragColor = mapColor;
}