attribute vec4 Position; 
attribute vec4 SourceColor; // 2
varying vec4 DestinationColor; // 3

uniform mat4 Projection;
uniform mat4 Modelview;

void main(void) {
    
    gl_Position = Projection * Modelview * Position;

    DestinationColor = SourceColor;
}
