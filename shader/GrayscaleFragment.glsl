varying lowp vec2 TexCoordOut;
uniform sampler2D Texture;

uniform lowp float alpha;
uniform lowp float saturation;

// Values from "Graphics Shaders: Theory and Practice" by Bailey and Cunningham
const mediump vec3 luminanceWeighting = vec3(0.2125, 0.7154, 0.0721);

void main(void) {

    lowp vec4 textureColor = texture2D(Texture, TexCoordOut);
    lowp float luminance = dot(textureColor.rgb, luminanceWeighting);
    
    lowp vec3 greyScaleColor = vec3(luminance);
    lowp vec4 mapColor = vec4(mix(greyScaleColor, textureColor.rgb, saturation), textureColor.w);
    
    mapColor.a = mapColor.a * alpha;
    
    gl_FragColor = mapColor;
}