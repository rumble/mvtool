
varying lowp vec2 TexCoordOut;
uniform sampler2D Texture;
uniform lowp float alpha;

varying lowp vec2 oneStepBackTextureCoordinate;
varying lowp vec2 twoStepsBackTextureCoordinate;
varying lowp vec2 threeStepsBackTextureCoordinate;
varying lowp vec2 fourStepsBackTextureCoordinate;
varying lowp vec2 oneStepForwardTextureCoordinate;
varying lowp vec2 twoStepsForwardTextureCoordinate;
varying lowp vec2 threeStepsForwardTextureCoordinate;
varying lowp vec2 fourStepsForwardTextureCoordinate;

void main(void) {
        
    lowp vec4 fragmentColor = texture2D(Texture, TexCoordOut) * 0.18;
    fragmentColor += texture2D(Texture, oneStepBackTextureCoordinate) * 0.15;
    fragmentColor += texture2D(Texture, twoStepsBackTextureCoordinate) *  0.12;
    fragmentColor += texture2D(Texture, threeStepsBackTextureCoordinate) * 0.09;
    fragmentColor += texture2D(Texture, fourStepsBackTextureCoordinate) * 0.05;
    fragmentColor += texture2D(Texture, oneStepForwardTextureCoordinate) * 0.15;
    fragmentColor += texture2D(Texture, twoStepsForwardTextureCoordinate) *  0.12;
    fragmentColor += texture2D(Texture, threeStepsForwardTextureCoordinate) * 0.09;
    fragmentColor += texture2D(Texture, fourStepsForwardTextureCoordinate) * 0.05;
    
    gl_FragColor = fragmentColor;
}