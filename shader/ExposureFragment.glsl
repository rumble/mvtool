
varying lowp vec2 TexCoordOut;
uniform sampler2D Texture;
uniform lowp float alpha;

uniform highp float exposure;

lowp vec4 getTextureColor(lowp vec4 origin, lowp float exposure) {
    
    //lowp vec4 textureColor = vec4(origin.rgb * pow(2.0, exposure), origin.w);
    lowp vec4 textureColor;
    textureColor = vec4((origin.rgb + vec3(exposure)), origin.w);

    return textureColor;
}

void main(void) {
        
    lowp vec4 textureColor = texture2D(Texture, TexCoordOut);
    
    //gl_FragColor = vec4(textureColor.rgb * pow(2.0, exposure), textureColor.w);
    
    
    gl_FragColor = getTextureColor(textureColor, exposure);
}