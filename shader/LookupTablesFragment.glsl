
varying lowp vec2 TexCoordOut;

uniform sampler2D Texture;
uniform sampler2D Texture2;

uniform lowp float alpha;

void main(void) {
        
    highp vec4 textureColor = texture2D(Texture, TexCoordOut);
    
    highp float b1 = floor(textureColor.b * 16.0);
    highp float b2 = ceil(textureColor.b * 16.0);
    
    highp vec2 texPos1;
    texPos1.y = textureColor.g;
    texPos1.x = (b1 /17.0 + 0.5 / 289.0 + (1.0 / 17.0 - 1.0 / 289.0) * textureColor.r);
    
    highp vec2 texPos2;
    texPos2.y = textureColor.g;
    texPos2.x = (b2 /17.0 + 0.5 / 289.0 + (1.0 / 17.0 - 1.0 / 289.0) * textureColor.r);
    
    lowp vec4 newColor1 = texture2D(Texture2, texPos1);
    lowp vec4 newColor2 = texture2D(Texture2, texPos2);
                                   
    lowp vec4 mapColor = mix(newColor1, newColor2, fract(textureColor.b * 16.0));
                                   
    mapColor = mapColor * alpha;
    
    gl_FragColor = mapColor;
}