
varying lowp vec2 TexCoordOut;
uniform sampler2D Texture;
uniform lowp float alpha;

void main(void) {
        
    lowp vec4 textureColor = texture2D(Texture, TexCoordOut);
    
    textureColor.a = textureColor.a * alpha;
    
    gl_FragColor = textureColor;
}