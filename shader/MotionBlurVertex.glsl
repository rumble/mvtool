attribute vec4 Position; 

uniform mat4 Projection;
uniform mat4 Modelview;

attribute vec2 TexCoordIn;
varying vec2 TexCoordOut;

uniform lowp vec2 directionalTexelStep;

varying lowp vec2 oneStepBackTextureCoordinate;
varying lowp vec2 twoStepsBackTextureCoordinate;
varying lowp vec2 threeStepsBackTextureCoordinate;
varying lowp vec2 fourStepsBackTextureCoordinate;
varying lowp vec2 oneStepForwardTextureCoordinate;
varying lowp vec2 twoStepsForwardTextureCoordinate;
varying lowp vec2 threeStepsForwardTextureCoordinate;
varying lowp vec2 fourStepsForwardTextureCoordinate;

void main(void) {
    
    gl_Position = Projection * Modelview * Position;
    
    TexCoordOut = TexCoordIn.xy;
    oneStepBackTextureCoordinate = TexCoordIn.xy - directionalTexelStep;
    twoStepsBackTextureCoordinate = TexCoordIn.xy - 2.0 * directionalTexelStep;
    threeStepsBackTextureCoordinate = TexCoordIn.xy - 3.0 * directionalTexelStep;
    fourStepsBackTextureCoordinate = TexCoordIn.xy - 4.0 * directionalTexelStep;
    oneStepForwardTextureCoordinate = TexCoordIn.xy + directionalTexelStep;
    twoStepsForwardTextureCoordinate = TexCoordIn.xy + 2.0 * directionalTexelStep;
    threeStepsForwardTextureCoordinate = TexCoordIn.xy + 3.0 * directionalTexelStep;
    fourStepsForwardTextureCoordinate = TexCoordIn.xy + 4.0 * directionalTexelStep;
}