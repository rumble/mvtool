//
//  AppDelegate.h
//  mvtool
//
//  Created by rumble on 14-4-23.
//  Copyright (c) 2014年 PixShow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
