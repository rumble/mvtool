//
//  MTUtility.m
//  mvtool
//
//  Created by rumble on 14/6/25.
//  Copyright (c) 2014年 PixShow. All rights reserved.
//

#import "MTUtility.h"

@implementation MTUtility

UIImage *MTCreateRepeatImage(UIImage *contentImage, CGSize imageSize, CGSize contentSize)
{
    //crop
    //根据图片宽高比，计算图片显示图片区域
    float contentRatio = contentSize.width / contentSize.height;
    float photoRatio = (contentImage.size.width / contentImage.size.height);
    float ratio = photoRatio / contentRatio;

    
    CGSize cropSize = CGSizeZero;
    
    if (ratio >= 1) {
        cropSize = CGSizeMake(ratio * contentSize.width , contentSize.height);
    }else {
        cropSize = CGSizeMake(contentSize.width , contentSize.height / ratio);
    }
    
    //合成图片
    UIGraphicsBeginImageContext(contentSize);
    
    CGFloat photoX = (contentSize.width - cropSize.width) / 2;
    CGFloat photoY = (contentSize.height - cropSize.height) / 2;

    NSLog(@"contentImagephotoX:%f,%f,%f,%f,%f", photoX, photoY,ratio, contentRatio, photoRatio);
    //画照片
    [contentImage drawInRect:CGRectMake(photoX, photoY, cropSize.width, cropSize.height)];
    
    UIImage *cropImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    //合成图片
    UIGraphicsBeginImageContext(imageSize);
    

    photoX = (imageSize.width - contentSize.width) / 2;
    photoY = (imageSize.height - contentSize.height) / 2;
    //画照片
    [cropImage drawInRect:CGRectMake(photoX, photoY, contentSize.width, contentSize.height)];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}

UIImage *MTCreateCurlPageImage(UIImage *contentImage, CGRect frame, CGFloat rotation)
{
    UIImageView *bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 320)];
    [bgImageView setImage:[UIImage imageNamed:@"curlPageBg.png"]];
    
    UIImageView *photoImageView = [[UIImageView alloc] initWithFrame:frame];
    [photoImageView setImage:contentImage];
    
    photoImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    photoImageView.layer.borderWidth = 5;
    photoImageView.layer.shadowColor = [UIColor grayColor].CGColor;
    photoImageView.layer.shadowOffset = CGSizeMake(0, 5);
    photoImageView.layer.shadowOpacity = 0.5;
    photoImageView.layer.shadowRadius = 5;
    photoImageView.layer.affineTransform = CGAffineTransformMakeRotation(rotation);

    [bgImageView addSubview:photoImageView];
    
    //UIGraphicsBeginImageContext(bgView.bounds.size);
    UIGraphicsBeginImageContext(bgImageView.frame.size);
    [bgImageView.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}
@end
