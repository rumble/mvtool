//
//  MTUtility.h
//  mvtool
//
//  Created by rumble on 14/6/25.
//  Copyright (c) 2014年 PixShow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTUtility : NSObject
UIImage *MTCreateRepeatImage(UIImage *contentImage, CGSize imageSize, CGSize contentSize);
UIImage *MTCreateCurlPageImage(UIImage *contentImage, CGRect frame, CGFloat rotation);
@end
