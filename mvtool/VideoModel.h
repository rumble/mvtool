//
//  VideoModel.h
//  mvtool
//
//  Created by rumble on 14/6/6.
//  Copyright (c) 2014年 PixShow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoModel : NSObject

+ (NSArray *)animaDetailConf4Memory:(NSInteger)photoCount;
+ (NSArray *)videoFragmentArray4Memory:(CGFloat)videoEndTime;
+ (CGFloat)videoDuration4Memory:(NSInteger)photoCount;

+ (NSArray *)animaDetailConf4Sport:(NSInteger)photoCount;
+ (NSArray *)videoFragmentArray4Sport:(CGFloat)videoEndTime;
+ (CGFloat)videoDuration4Sport:(NSInteger)photoCount;

+ (NSArray *)animaDetailConf4Photo:(NSInteger)photoCount;
+ (NSArray *)videoFragmentArray4Photo:(CGFloat)videoEndTime;
+ (CGFloat)videoDuration4Photo:(NSInteger)photoCount;

+ (NSArray *)animaDetailConf4CurlPage:(NSInteger)photoCount;
+ (NSArray *)videoFragmentArray4CurlPage:(CGFloat)videoEndTime;
+ (CGFloat)videoDuration4CurlPage:(NSInteger)photoCount;
@end
