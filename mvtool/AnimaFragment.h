//
//  AnimaFragment.h
//  mvtool
//
//  Created by rumble on 14-6-6.
//  Copyright (c) 2014年 PixShow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OpenGLView.h"

@interface AnimaFragment : NSObject

@property (nonatomic) float             duration;
@property (nonatomic) AnimationType     animationType;
@property (nonatomic) id                animationParam;
@property (nonatomic) FilterType        filterType;
@property (nonatomic) id                filterParam;
@property (nonatomic) TextureInfo       filterTextureInfo;
@property (nonatomic) ShaderParams      shaderParams;
@property (nonatomic) ShaderParams      shaderParams2;
@property (nonatomic) NSArray           *positions;
@property (nonatomic) id                quadParam;
@property (nonatomic) id                enlargeParam;
@property (nonatomic) id                imageRepeatParam;
@end
