//
//  AnimaDetail.h
//  mvtool
//
//  Created by rumble on 14-6-6.
//  Copyright (c) 2014年 PixShow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AnimaDetail : NSObject

@property (nonatomic) float     startTime;
@property (nonatomic) float     endTime;
@property (nonatomic) NSArray   *animaFragments;
@end
