//
//  AppDelegate.m
//  mvtool
//
//  Created by rumble on 14-4-23.
//  Copyright (c) 2014年 PixShow. All rights reserved.
//

#import "AppDelegate.h"
#import "MTHomeController.h"
#import <AssetsLibrary/AssetsLibrary.h>

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    
    MTHomeController *hc = [MTHomeController new];
    self.window.rootViewController = hc;
    
    //[self calcColorFile];
    
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)calcColorFile
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"c1.13" ofType:nil];
    
    NSString *pixelFile = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    
    NSString *sep = @"\n";
    NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:sep];
    NSArray *pixelArray = [pixelFile componentsSeparatedByCharactersInSet:set];
    //NSLog(@"pixelArray:\n%@", pixelArray);
    
    
    const size_t BlockNum = 17;
    const size_t Width = 17;
    const size_t Height = 17;
    const size_t Area = Width * Height;
    const size_t ComponentsPerPixel = 4; // rgba
    const size_t BitsPerComponent = 8;
    
    NSLog(@"Start");
    for (size_t num = 0; num < BlockNum; num++) {
        uint8_t pixelData[Area * ComponentsPerPixel];

        
        for(size_t row = 0; row < Height; row++)
        {
            for(size_t col = 0; col < Width; col++)
            {
                
                UInt8* pixel = &pixelData[(row * Width + col) * ComponentsPerPixel];
                
                NSString *pixelString = pixelArray[num * Width * Height + row * Width + col];
                NSArray *thePixelArray = [pixelString componentsSeparatedByString:@" "];
                pixel[0] = floor([thePixelArray[0] floatValue] * 255.0 + 0.5);
                pixel[1] = floor([thePixelArray[1] floatValue] * 255.0 + 0.5);
                pixel[2] = floor([thePixelArray[2] floatValue] * 255.0 + 0.5);
                pixel[3] = 255; // opaque
                
                // create the bitmap context:
                const size_t BytesPerRow=  Width * ComponentsPerPixel;
                CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
                CGContextRef gtx = CGBitmapContextCreate(&pixelData[0], Width, Height, BitsPerComponent, BytesPerRow, colorSpace, (CGBitmapInfo)kCGImageAlphaPremultipliedLast);
                // create the image:
                CGImageRef toCGImage = CGBitmapContextCreateImage(gtx);
                UIImage * uiimage = [[UIImage alloc] initWithCGImage:toCGImage];
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,        NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString *savedImagePath = [documentsDirectory  stringByAppendingPathComponent:[NSString stringWithFormat:@"%i.png", num]];
                UIImage *image = uiimage; // imageView is my image from camera
                NSData *imageData = UIImagePNGRepresentation(image);
                [imageData writeToFile:savedImagePath atomically:NO];
                //UIImageWriteToSavedPhotosAlbum(uiimage, nil, nil, nil);
            }
        }
    }
    
    NSLog(@"Done");
}

- (void)testLookupData
{
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"c1.3" ofType:nil];
    
    NSString *pixelFile = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    
    NSString *sep = @"\n";
    NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:sep];
    NSArray *pixelArray = [pixelFile componentsSeparatedByCharactersInSet:set];
    NSLog(@"pixelArray_count:%i", pixelArray.count);
    
    UIImage *testImage = [UIImage imageNamed:@"test.JPG"];
    CGImageRef cgimage = testImage.CGImage;
    
    float pixRGB[4] = {0};
    
    size_t width  = CGImageGetWidth(cgimage);
    size_t height = CGImageGetHeight(cgimage);
    
    size_t bpr = CGImageGetBytesPerRow(cgimage);
    size_t bpp = CGImageGetBitsPerPixel(cgimage);
    size_t bpc = CGImageGetBitsPerComponent(cgimage);
    size_t bytes_per_pixel = bpp / bpc;
    
    CGBitmapInfo info = CGImageGetBitmapInfo(cgimage);
    CGImageAlphaInfo alphaInfo = CGImageGetAlphaInfo(cgimage);
    NSLog(
          @"\n"
          "==========\n"
          "CGImageGetHeight: %d\n"
          "CGImageGetWidth:  %d\n"
          "CGImageGetColorSpace: %@\n"
          "CGImageGetBitsPerPixel:     %d\n"
          "CGImageGetBitsPerComponent: %d\n"
          "CGImageGetBytesPerRow:      %d\n"
          "CGImageGetBitmapInfo: 0x%.8X\n"
          "  kCGBitmapAlphaInfoMask     = %s\n"
          "  kCGBitmapFloatComponents   = %s\n"
          "  kCGBitmapByteOrderMask     = %s\n"
          "  kCGBitmapByteOrderDefault  = %s\n"
          "  kCGBitmapByteOrder16Little = %s\n"
          "  kCGBitmapByteOrder32Little = %s\n"
          "  kCGBitmapByteOrder16Big    = %s\n"
          "  kCGBitmapByteOrder32Big    = %s\n"
          "  alphaInfo                  = %i\n",
          (int)height,
          (int)width,
          CGImageGetColorSpace(cgimage),
          (int)bpp,
          (int)bpc,
          (int)bpr,
          (unsigned)info,
          (info & kCGBitmapAlphaInfoMask)     ? "YES" : "NO",
          (info & kCGBitmapFloatComponents)   ? "YES" : "NO",
          (info & kCGBitmapByteOrderMask)     ? "YES" : "NO",
          (info & kCGBitmapByteOrderDefault)  ? "YES" : "NO",
          (info & kCGBitmapByteOrder16Little) ? "YES" : "NO",
          (info & kCGBitmapByteOrder32Little) ? "YES" : "NO",
          (info & kCGBitmapByteOrder16Big)    ? "YES" : "NO",
          (info & kCGBitmapByteOrder32Big)    ? "YES" : "NO",
          alphaInfo
          );
    CFDataRef dataRef = CGDataProviderCopyData(CGImageGetDataProvider(cgimage));
    CFMutableDataRef m_DataRef = CFDataCreateMutableCopy(0, 0,  dataRef);
	UInt8 *bytes = (UInt8 *) CFDataGetMutableBytePtr(m_DataRef);
    size_t len = width * height * bytes_per_pixel * sizeof(UInt8);
    UInt8 *cpBytes = malloc(len);
    int min = 255, max = 0;
    
    BOOL infoMaskOnly = YES;
    if (info & kCGBitmapFloatComponents) {
        infoMaskOnly = NO;
    }else if (info & kCGBitmapByteOrderMask) {
        infoMaskOnly = NO;
    }else if (info & kCGBitmapByteOrderDefault) {
        infoMaskOnly = NO;
    }else if (info & kCGBitmapByteOrder16Little) {
        infoMaskOnly = NO;
    }else if (info & kCGBitmapByteOrder32Little) {
        infoMaskOnly = NO;
    }else if (info & kCGBitmapByteOrder16Big) {
        infoMaskOnly = NO;
    }else if (info & kCGBitmapByteOrder32Big) {
        infoMaskOnly = NO;
    }
    
    BOOL orderRGB = YES;
    if (alphaInfo == kCGImageAlphaPremultipliedLast || alphaInfo == kCGImageAlphaLast || alphaInfo == kCGImageAlphaNoneSkipLast) {
        orderRGB = YES;
    }else if(alphaInfo == kCGImageAlphaPremultipliedFirst || alphaInfo == kCGImageAlphaFirst || alphaInfo == kCGImageAlphaNoneSkipFirst) {
        orderRGB = NO;
    }

    for(size_t row = 0; row < height; row++)
    {
        for(size_t col = 0; col < width; col++)
        {
            UInt8* pixel =
            &bytes[row * bpr + col * bytes_per_pixel];
            for(size_t x = 0; x < bytes_per_pixel; x++)
            {
                pixRGB[x] = pixel[x];
            }
            
            NSInteger R = 0;
            NSInteger G = 0;
            NSInteger B = 0;
            NSInteger A = 0;
            
            if (orderRGB) {
                R = pixRGB[0];
                G = pixRGB[1];
                B = pixRGB[2];
                A = pixRGB[3];
            }else if (infoMaskOnly) {
                R = pixRGB[1];
                G = pixRGB[2];
                B = pixRGB[3];
                A = pixRGB[0];
            }else{
                R = pixRGB[2];
                G = pixRGB[1];
                B = pixRGB[0];
                A = pixRGB[3];
            }
            
            R = MIN(R / 15, 16);
            G = MIN(G / 15, 16);
            B = MIN(B / 15, 16);
            
            NSString *pixelString = pixelArray[17 * 17 * B + 17 * G + R];
            NSArray *thePixelArray = [pixelString componentsSeparatedByString:@" "];
            
            NSInteger satuR = [thePixelArray[0] floatValue] * 255;
            NSInteger satuG = [thePixelArray[1] floatValue] * 255;
            NSInteger satuB = [thePixelArray[2] floatValue] * 255;
            
            if (orderRGB) {
                pixel[0] = satuR;
                pixel[1] = satuG;
                pixel[2] = satuB;
                
            }else if (infoMaskOnly) {
                pixel[1] = satuR;
                pixel[2] = satuG;
                pixel[3] = satuB;
                
            }else {
                pixel[2] = satuR;
                pixel[1] = satuG;
                pixel[0] = satuB;
            }
        }
    }
    
    CGContextRef ctx = CGBitmapContextCreate(bytes,
											 CGImageGetWidth(cgimage),
											 CGImageGetHeight(cgimage),
											 CGImageGetBitsPerComponent(cgimage),
											 CGImageGetBytesPerRow(cgimage),
											 CGColorSpaceCreateDeviceRGB(),
											 CGImageGetBitmapInfo(cgimage)
											 );
	
	CGImageRef imageRef = CGBitmapContextCreateImage(ctx);
	CGContextRelease(ctx);
	UIImage *finalImage = [UIImage imageWithCGImage:imageRef];
	CGImageRelease(imageRef);
	CFRelease(m_DataRef);
    CFRelease(dataRef);
    free(cpBytes);
    
    UIImageWriteToSavedPhotosAlbum(finalImage, nil, nil, nil);
}
@end
