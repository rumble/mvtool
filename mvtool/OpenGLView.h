//
//  OpenGLView.h
//  HelloOpenGL
//
//  Created by Ray Wenderlich on 5/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#include <OpenGLES/ES2/gl.h>
#include <OpenGLES/ES2/glext.h>
#import <AVFoundation/AVFoundation.h>
#import "CC3Foundation.h"

typedef enum {
    kMVTypeMemory,
    kMVTypeSport,
    kMVTypePhoto,
    KMVTypeCurlPage,
    NUM_MVTYPE,
}MVType;

typedef enum {
    kFilterTypeNone,
    kFilterTypeLomo,
    kFilterTypeXpro,
    kFilterTypeGrayscale,
    kFilterTypeLookup,
    kFilterTypeMotionBlur,
    kFilterTypeExposure,
    kFilterTypeChangeAlpha,
    kFilterTypeExposureLight,
    NUM_FILTER_TYPE,
} FilterType;

typedef enum {
    kAnimationTypeNone,
    kAnimationTypeQuadrangle,
    kAnimationTypeExpand,
    kAnimationTypeSlide,
    kAnimationTypeCurl,
    NUM_ANIMATION_TYPE,
} AnimationType;

//图片产生一个TEXTURE, 信息包括id和size
typedef struct {
    GLuint textureId;
    CGSize size;
} TextureInfo;

/*****
 动画片段. 包括:动画时长, 动画类型, 使用的滤镜类型 以及空间移动数组。
 
 空间移动数组。 按时间等分。 例:
 duration = 4; postions = {0,0,0, 1,0,0, 0,1,0, 0,0,1, 0,0,0};
 意思就是 第0秒时在{0,0,0}; 第1秒在{1,0,0}; 第2秒在{0,1,0}; 第3秒在{0,0,1}; 第4秒在{0,0,0}.
*****/

//每张图片都会在视频中存在一段时间，做一系列动作。

#define Endless -1

//Shader所需参数
enum
{
    UNIFORM_PROJECTION,
    UNIFORM_MODELVIEW,
    UNIFORM_TEXTURE,
    UNIFORM_TEXTURE1,
    UNIFORM_ALPHA,
    UNIFORM_SATURATION,
    UNIFORM_MOTION_BLUR_STEP,
    UNIFORM_EXPOSURE,
    UNIFORM_CYLINDERPOSITION,
    UNIFORM_CYLINDERDIRECTION,
    UNIFORM_CYLINDERRADIUS,
    UNIFORM_BACKGRADIENT,
    NUM_UNIFORMS
};

enum
{
    ATTRIBS_VERTEX,
    ATTRIBS_TEXCOORD,
    ATTRIBS_SOURCECOLOR,
    NUM_ATTRIBS
};

typedef struct {
    GLuint  programHandle;
    GLint   uniforms[NUM_UNIFORMS];
    GLuint  attribs[NUM_ATTRIBS];
} ShaderParams;

@class VideoFragment, tex;
@interface OpenGLView : UIView {
    
    CAEAGLLayer* _eaglLayer;
    EAGLContext* _context;
    GLuint  _framebuffer;
    GLuint _colorRenderBuffer;
    GLuint _sampleFramebuffer;
    GLuint _sampleColorRenderbuffer;
    GLuint _depthRenderBuffer;
    GLuint elementCount; //Number of entries in the index buffer
    
    CFTimeInterval _currentTime;
    
    CGFloat _currentRotation;
    
    CMSampleBufferRef sampleBuffer;
    
    NSURL *movieURL;
    NSString *fileType;
	AVAssetWriter *assetWriter;
	AVAssetWriterInput *assetWriterVideoInput;
    AVAssetWriterInput *assetWriterAudioInput;
    AVAssetWriterInputPixelBufferAdaptor *assetWriterPixelBufferInput;
	dispatch_queue_t movieWritingQueue;
    
    GLuint movieFramebuffer, movieRenderbuffer;
    CVPixelBufferRef renderTarget;
    CVOpenGLESTextureRef renderTexture;
    CVOpenGLESTextureCacheRef videoTextureCache;
    
    AVAssetReader *audioReader;
    AVAssetReaderOutput *readerOutput;
    
    ShaderParams simpleShaderParams;
    ShaderParams motionBlurShaderParmas;
    ShaderParams exposureShaderParmas;
    ShaderParams colorShaderParmas;
    ShaderParams grayscaleParmas;
    ShaderParams frontVertexParmas;
    ShaderParams backVertexParmas;
    
    CGFloat videoSumDuartion;
    GLint viewportWidth, viewportHeight;
}

@property (nonatomic, strong)NSArray        *photosArray;
@property (nonatomic, strong)NSArray        *overlayVideosArray;
@property (nonatomic, strong)AVPlayer       *player;
@property (nonatomic, strong)NSArray        *imagesArray;
@property (nonatomic, strong)CADisplayLink  *displayLink;
@property (nonatomic, strong)NSArray        *videoFragmentArray;
@property (nonatomic, strong)NSArray        *overlayFragmentArray;

@property (nonatomic) MVType    mvType;
@property (nonatomic, readonly) BOOL antialiasing;

- (void)playDemoWithType:(MVType)mvType;
- (void)stopPlay;
- (void)startPlay;
- (void)setDisplayImages:(NSArray *)imageArray;
@end
