//
//  VideoModel.m
//  mvtool
//
//  Created by rumble on 14/6/6.
//  Copyright (c) 2014年 PixShow. All rights reserved.
//

#import "VideoModel.h"
#import "OpenGLView.h"

NSString *kImageKey = @"imageKey";
NSString *kPositonKey = @"kPositonKey";
NSString *kEnlargeKey = @"kEnlargeKey";
NSString *kImageRepeatKey = @"kImageRepeatKey";
NSString *kAnimaDetailKey = @"animaDetailKey";

NSString *kAnimaFragmentKey = @"animaFragmentKey";
NSString *kAnimationStartTimeKey = @"animationStartTimeKey";
NSString *kAnimationEndTimeKey = @"animationEndTimeKey";
NSString *kAnimaFragmentDurationKey = @"animaFragmentDurationKey";
NSString *kFilterTypeKey = @"filterTypeKey";
NSString *kFitlerParamKey = @"fitlerParamKey";
NSString *kQuadParamKey = @"quadParamKey";
NSString *kAmimationParamKey = @"amimationParamKey";
NSString *kAnimationTypeKey = @"animationTypeKey";
NSString *kFragmentEndTimeKey = @"kFragmentEndTimeKey";

NSString *kAnimaDetailSort = @"kAnimaDetailSort";
NSString *kAnimaDetailPhotoSort = @"kAnimaDetailPhotoSort";

static const CGFloat kWidth = 2;
static const CGFloat kHeight = 2;

@implementation VideoModel

#pragma mark - Memory
+ (NSArray *)animaDetailConf4Memory:(NSInteger)photoCount
{
    CGFloat hMargin = 0.06;
    CGFloat vMargin = 0.18;
    CGFloat vMargin1 = 0.30;
    CGFloat vMarginH = 0.32;
    CGFloat vMarginH1 = 0.4;
    CGFloat stayDuration = 1.7;
    CGFloat slideDuration = 0.2;
    CGFloat exposureDuration = 1;
    CGFloat exposureDuration2 = 0.3;
    CGFloat extraDuartion = 0.6;
    CGFloat zLevel = 0.2;
    
    NSArray *originalArray = @[
                               //1 blur
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(0),
                                           kAnimaFragmentKey : @[@{
                                                                    kAnimaFragmentDurationKey : @(slideDuration),
                                                                    kFilterTypeKey : @(kFilterTypeMotionBlur),
                                                                    kFitlerParamKey : @[@(10), @(0)],
                                                                    kAnimationTypeKey : @(kAnimationTypeNone),
                                                                    kPositonKey : @[@[@(0), @(0), @(zLevel)], @[@(-2), @(0), @(zLevel)]],
                                                                    }],
                                           }
                                   },
                               
                               //1
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(slideDuration + stayDuration),
                                           kAnimaFragmentKey : @[@{
                                                                    kAnimaFragmentDurationKey : @(slideDuration),
                                                                    kFilterTypeKey : @(kFilterTypeNone),
                                                                    kFitlerParamKey : @(kAnimationTypeNone),
                                                                    kPositonKey : @[@[@(2), @(0), @(zLevel)], @[@(-hMargin / 2), @(0), @(zLevel)]],
                                                                    },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(stayDuration),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kFitlerParamKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(-hMargin / 2), @(0), @(zLevel)], @[@(hMargin / 2), @(0), @(zLevel)]],
                                                                     },
                                                                @{
                                                                    kAnimaFragmentDurationKey : @(slideDuration),
                                                                    kFilterTypeKey : @(kFilterTypeMotionBlur),
                                                                    kFitlerParamKey : @[@(4), @(0)],
                                                                    kAnimationTypeKey : @(kAnimationTypeNone),
                                                                    kPositonKey : @[@[@(hMargin), @(0), @(zLevel)], @[@(-2), @(0), @(zLevel)]],
                                                                    }],
                                           }
                                   },
                               
                               //2
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(slideDuration + stayDuration),
                                           kAnimaFragmentKey : @[@{
                                                                    kAnimaFragmentDurationKey : @(slideDuration),
                                                                    kFilterTypeKey : @(kFilterTypeMotionBlur),
                                                                    kAnimationTypeKey : @(kAnimationTypeNone),
                                                                    kPositonKey : @[@[@(2 + hMargin), @(zLevel), @(0)], @[@(-hMargin / 2), @(0), @(zLevel)]],
                                                                    },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(stayDuration),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kFitlerParamKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(-hMargin / 2), @(0), @(zLevel)], @[@(hMargin / 2), @(0), @(zLevel)]],
                                                                     },
                                                                @{
                                                                    kAnimaFragmentDurationKey : @(slideDuration),
                                                                    kFilterTypeKey : @(kFilterTypeMotionBlur),
                                                                    kFitlerParamKey : @[@(4), @(0)],
                                                                    kAnimationTypeKey : @(kAnimationTypeNone),
                                                                    kPositonKey : @[@[@(hMargin), @(0), @(zLevel)], @[@(-2), @(0), @(zLevel)]],
                                                                    }],
                                           }
                                   },
                               
                               //3
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(slideDuration + stayDuration),
                                           kAnimaFragmentKey : @[@{
                                                                    kAnimaFragmentDurationKey : @(slideDuration),
                                                                    kFilterTypeKey : @(kFilterTypeMotionBlur),
                                                                    kAnimationTypeKey : @(kAnimationTypeNone),
                                                                    kPositonKey : @[@[@(2 + hMargin), @(0), @(zLevel)], @[@(-hMargin / 2), @(0), @(zLevel)]],
                                                                    },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(stayDuration),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kFitlerParamKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(-hMargin / 2), @(0), @(zLevel)], @[@(hMargin / 2), @(0), @(zLevel)]],
                                                                     },
                                                                @{
                                                                    kAnimaFragmentDurationKey : @(slideDuration),
                                                                    kFilterTypeKey : @(kFilterTypeMotionBlur),
                                                                    kFitlerParamKey : @[@(4), @(0)],
                                                                    kAnimationTypeKey : @(kAnimationTypeNone),
                                                                    kPositonKey : @[@[@(hMargin), @(0), @(zLevel)], @[@(-2), @(0), @(zLevel)]],
                                                                    }],
                                           }
                                   },
                               
                               //4
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(slideDuration + stayDuration + extraDuartion),
                                           kAnimaFragmentKey : @[@{
                                                                    kAnimaFragmentDurationKey : @(slideDuration),
                                                                    kFilterTypeKey : @(kFilterTypeMotionBlur),
                                                                    kAnimationTypeKey : @(kAnimationTypeNone),
                                                                    kPositonKey : @[@[@(2), @(0), @(zLevel)], @[@(hMargin), @(0), @(zLevel)]],
                                                                    },
                                                                @{
                                                                    kAnimaFragmentDurationKey : @(stayDuration + extraDuartion),
                                                                    kFilterTypeKey : @(kFilterTypeNone),
                                                                    kAnimationTypeKey : @(kAnimationTypeNone),
                                                                    kPositonKey : @[@[@(hMargin - 0.01), @(0), @(zLevel)], @[@(-hMargin + 0.01), @(0), @(zLevel)]],
                                                                    },
                                                                 
                                                                @{
                                                                    kAnimaFragmentDurationKey : @(slideDuration),
                                                                    kFilterTypeKey : @(kFilterTypeMotionBlur),
                                                                    kFitlerParamKey : @[@(0), @(4)],
                                                                    kAnimationTypeKey : @(kAnimationTypeNone),
                                                                    kPositonKey : @[@[@(-hMargin), @(0), @(zLevel)], @[@(0), @(2), @(zLevel)]],
                                                                    }],
                                           }
                                   },
                               
                               //5
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(0),
                                           kAnimaFragmentKey : @[@{
                                                                    kAnimaFragmentDurationKey : @(slideDuration),
                                                                    kFilterTypeKey : @(kFilterTypeMotionBlur),
                                                                    kAnimationTypeKey : @(kAnimationTypeNone),
                                                                    kPositonKey : @[@[@(0), @(-2), @(zLevel)], @[@(0), @(vMargin), @(zLevel)]],
                                                                    },
                                                                @{
                                                                    kAnimaFragmentDurationKey : @(stayDuration),
                                                                    kFilterTypeKey : @(kFilterTypeNone),
                                                                    kAnimationTypeKey : @(kAnimationTypeNone),
                                                                    kPositonKey : @[@[@(0), @(vMargin), @(zLevel)], @[@(0), @(vMargin1), @(zLevel)]],
                                                                    },
                                                                @{
                                                                    kAnimaFragmentDurationKey : @(slideDuration),
                                                                    kFilterTypeKey : @(kFilterTypeMotionBlur),
                                                                    kFitlerParamKey : @[@(0), @(4)],
                                                                    kAnimationTypeKey : @(kAnimationTypeNone),
                                                                    kPositonKey : @[@[@(0), @(vMargin1), @(zLevel)], @[@(0), @(2), @(zLevel)]],
                                                                    }],
                                           }
                                   },
                               
                               //6
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(slideDuration + stayDuration),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(slideDuration),
                                                                     kFilterTypeKey : @(kFilterTypeMotionBlur),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(-4), @(zLevel)], @[@(0), @(-2 + vMargin), @(zLevel)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(stayDuration),
                                                                     kFilterTypeKey : @(kFilterTypeMotionBlur),
                                                                     kFitlerParamKey : @[@(0), @(2)],
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(-2 + vMargin), @(zLevel)], @[@(0), @(-2 + vMargin1), @(zLevel)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(slideDuration),
                                                                     kFilterTypeKey : @(kFilterTypeMotionBlur),
                                                                     kFitlerParamKey : @[@(0), @(4)],
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(-2 + vMargin1), @(zLevel)], @[@(0), @(vMargin), @(zLevel)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(stayDuration),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(vMargin), @(zLevel)], @[@(0), @(vMargin1), @(zLevel)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(slideDuration),
                                                                     kFilterTypeKey : @(kFilterTypeMotionBlur),
                                                                     kFitlerParamKey : @[@(0), @(4)],
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(vMargin1), @(zLevel)], @[@(0), @(2), @(zLevel)]],
                                                                     }],
                                           }
                                   },
                               
                               //7
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(slideDuration + stayDuration),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(slideDuration),
                                                                     kFilterTypeKey : @(kFilterTypeMotionBlur),
                                                                     kFitlerParamKey : @[@(0), @(4)],
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(-4), @(zLevel)], @[@(0), @(-2 + vMargin), @(zLevel)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(stayDuration),
                                                                     kFilterTypeKey : @(kFilterTypeMotionBlur),
                                                                     kFitlerParamKey : @[@(0), @(2)],
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(-2 + vMargin), @(zLevel)], @[@(0), @(-2 + vMargin1), @(zLevel)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(slideDuration),
                                                                     kFilterTypeKey : @(kFilterTypeMotionBlur),
                                                                     kFitlerParamKey : @[@(0), @(4)],
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(-2 + vMargin1), @(zLevel)], @[@(0), @(vMarginH), @(zLevel)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(stayDuration),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(vMarginH), @(zLevel)], @[@(0), @(vMarginH1), @(zLevel)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(slideDuration),
                                                                     kFilterTypeKey : @(kFilterTypeMotionBlur),
                                                                     kFitlerParamKey : @[@(0), @(4)],
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(vMarginH1), @(zLevel)], @[@(0), @(2), @(zLevel)]],
                                                                     }],
                                           }
                                   },
                               
                               //8
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(2 * slideDuration + 2 * stayDuration),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(slideDuration),
                                                                     kFilterTypeKey : @(kFilterTypeMotionBlur),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(-4), @(zLevel)], @[@(hMargin), @(-2 + vMarginH), @(zLevel)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(stayDuration),
                                                                     kFilterTypeKey : @(kAnimationTypeNone),
                                                                     kFitlerParamKey : @[@(0), @(2)],
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(hMargin), @(-2 + vMarginH), @(zLevel)], @[@(-hMargin), @(-2 + vMarginH1), @(zLevel)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(slideDuration),
                                                                     kFilterTypeKey : @(kFilterTypeMotionBlur),
                                                                     kFitlerParamKey : @[@(0), @(4)],
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(-hMargin), @(-2 + vMarginH1), @(zLevel)], @[@(hMargin / 2), @(0), @(zLevel)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(stayDuration),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(zLevel)], @[@(-hMargin / 2), @(0), @(zLevel)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(slideDuration),
                                                                     kFilterTypeKey : @(kFilterTypeMotionBlur),
                                                                     kFitlerParamKey : @[@(4), @(0)],
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(-hMargin), @(0), @(zLevel)], @[@(2), @(0), @(zLevel)]],
                                                                     }],
                                           }
                                   },
                               //9
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(slideDuration + stayDuration),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(slideDuration),
                                                                     kFilterTypeKey : @(kFilterTypeMotionBlur),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(-hMargin - 2), @(0), @(zLevel)], @[@(0), @(0), @(zLevel)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(exposureDuration2),
                                                                     kFilterTypeKey : @(kFilterTypeExposureLight),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(zLevel)], @[@(0), @(0), @(zLevel)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(stayDuration - exposureDuration2 - exposureDuration),
                                                                     kFilterTypeKey : @(kAnimationTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(zLevel)], @[@(0), @(0), @(zLevel)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(exposureDuration),
                                                                     kFilterTypeKey : @(kFilterTypeExposure),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(zLevel)], @[@(0), @(0), @(zLevel)]],
                                                                     },
                                                                 ],
                                           }
                                   },
                               ];
    
    NSArray *indexArray = [NSArray array];
    
    if (6 == photoCount) {
        indexArray = @[@(0), @(1), @(4), @(5), @(8), @(9)];
    }else if(7 == photoCount) {
        indexArray = @[@(0), @(1), @(2), @(4), @(5), @(8), @(9)];
    }else if(8 == photoCount) {
        indexArray = @[@(0), @(1), @(2), @(4), @(5), @(6), @(8), @(9)];
    }else if(9 == photoCount) {
        indexArray = @[@(0), @(1), @(2), @(3), @(4), @(5), @(6), @(8), @(9)];
    }else if(10 == photoCount) {
        indexArray = @[@(0), @(1), @(2), @(3), @(4), @(5), @(6), @(7), @(8), @(9)];
    }
    
    CGFloat tempTime = 0;
    
    NSMutableArray *selectedArray = [NSMutableArray array];
    
    NSInteger theIndex = -1;
    for (NSNumber *index in indexArray) {
        theIndex++;
        //start time
        NSMutableDictionary *mutableDic = [NSMutableDictionary dictionaryWithDictionary:originalArray[index.integerValue]];
        NSMutableDictionary *animaDetailDic = [NSMutableDictionary dictionaryWithDictionary:mutableDic[kAnimaDetailKey]];

        [animaDetailDic setValue:@(tempTime) forKey:kAnimationStartTimeKey];
        
        //end tiem
        NSArray *animaFragmentArray = animaDetailDic[kAnimaFragmentKey];
        CGFloat tempEndTime = 0;
        for (NSDictionary *animaFragmentDic in animaFragmentArray) {
            
            NSNumber *fragmentDuration = animaFragmentDic[kAnimaFragmentDurationKey];
            tempEndTime += fragmentDuration.floatValue;
        }
        [animaDetailDic setValue:@(tempEndTime + tempTime) forKey:kAnimationEndTimeKey];
        [animaDetailDic setValue:@(theIndex) forKey:kAnimaDetailPhotoSort];
        //calc next videoFragment start time
        NSNumber *fragmentEndTime = animaDetailDic[kFragmentEndTimeKey];
        tempTime += fragmentEndTime.floatValue;

        
        [mutableDic setValue:animaDetailDic forKey:kAnimaDetailKey];
        [selectedArray addObject:mutableDic];
    
    }
    
    return selectedArray;
}

+ (NSArray *)videoFragmentArray4Memory:(CGFloat)videoEndTime
{
    
    CGFloat logoTime = 2.5;
    CGFloat Interval = 1.5;
    CGFloat overlayTime = 9;
    NSArray *originalArray = @[
                               //1
                               @{
                                   kAnimaDetailKey : @{
                                           kAnimationStartTimeKey : @(0),
                                           kAnimationEndTimeKey : @(MIN(overlayTime, videoEndTime - logoTime - Interval)),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(videoEndTime - logoTime - Interval),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)], @[@(0), @(0), @(0)]],
                                                                     kFitlerParamKey : @[@(0.05)],
                                                                     }],
                                           }
                                   },

                               //2
                               @{
                                   kAnimaDetailKey : @{
                                           kAnimationStartTimeKey : @(overlayTime),
                                           kAnimationEndTimeKey : @(videoEndTime - logoTime - Interval),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(videoEndTime - logoTime - Interval),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)], @[@(0), @(0), @(0)]],
                                                                     kFitlerParamKey : @[@(0.05)],
                                                                     }],
                                           }
                                   },
                               
                               //3
                               @{
                                   kAnimaDetailKey : @{
                                           kAnimationStartTimeKey : @(videoEndTime - logoTime),
                                           kAnimationEndTimeKey : @(videoEndTime),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(logoTime),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)], @[@(0), @(0), @(0)]],
                                                                     kFitlerParamKey : @[@(0.15)],
                                                                     },],
                                           }
                                   },
                               ];
    
    return originalArray;
}

+ (CGFloat)videoDuration4Memory:(NSInteger)photoCount
{
    CGFloat videoSumDuartion = 0;
    
    if (10 == photoCount) {
        videoSumDuartion = 19.5;
    }else if (9 == photoCount) {
        videoSumDuartion = 18;
    }else if (8 == photoCount) {
        videoSumDuartion = 16.2;
    }else if (7 == photoCount) {
        videoSumDuartion = 14.5;
    }else if (6 == photoCount) {
        videoSumDuartion = 13;
    }
    
    return videoSumDuartion;
}

#pragma mark - Sport
+ (NSArray *)animaDetailConf4Sport:(NSInteger)photoCount
{
    
    CGFloat quadRation = 0.8;
    CGFloat zLevel = 0.4;
    CGFloat vPadding = 0.1;
    CGFloat slideMin = 0.3;
    CGFloat slideMax = 0.6;
    CGFloat quadHeight = 1.2;
    
    NSArray *originalArray = @[
                               //1
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(1.7),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(0.2),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0.2), @(-0.2), @(1.8)], @[@(-0.1), @(0), @(0.6)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(1.3),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(-0.1), @(0), @(0.6)], @[@(0.05), @(0), @(zLevel)]],
                                                                     },
                                                                 
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(0.2),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0.05), @(0), @(zLevel)], @[@(0.05), @(0), @(zLevel)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(0.3),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0.05), @(0), @(zLevel)], @[@(2 -0.06), @(0), @(zLevel)]],
                                                                     }],
                                           }
                                   },
                               //2
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(0.6),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(0.3),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(-2 + 0.05), @(0), @(zLevel)], @[@(-0.06), @(0), @(zLevel)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(1),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(-0.06), @(0), @(zLevel)], @[@(0.06), @(0), @(zLevel-0.02)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(0.3),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0.06), @(0), @(zLevel-0.02)], @[@(0.06), @(0), @(zLevel-0.02)]],
                                                                     },
                                                                 ]
                                           }
                                   },
                               //3
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(0.4),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(0.5),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeExpand),
                                                                     kPositonKey : @[@[@(0), @(0), @(zLevel-0.01)], @[@(0), @(0), @(zLevel-0.01)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(1.4),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(zLevel-0.015)], @[@(0), @(0), @(zLevel-0.015)]],
                                                                     },
                                                                 ]
                                           }
                                   },
                               //4
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(0.5),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(0.5),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeSlide),
                                                                     kPositonKey : @[@[@(0), @(1 - slideMin), @(zLevel)]],
                                                                     kQuadParamKey : @[@[@(1),@(-slideMin),@(0)], @[@(1),@(slideMin),@(0)],@[@(1),@(slideMin),@(0)],@[@(1),@(-slideMax),@(0)],@[@(1),@(-slideMin),@(0)], @[@(1),@(slideMin),@(0)],@[@(-1),@(slideMin),@(0)],@[@(-1),@(-slideMax),@(0)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(0.8),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeQuadrangle),
                                                                     kQuadParamKey : @[@[@(1),@(-slideMin),@(0)], @[@(1),@(slideMin),@(0)],@[@(-1),@(slideMin),@(0)],@[@(-1),@(-slideMax),@(0)]],
                                                                     kPositonKey : @[@[@(0), @(1 - slideMin), @(zLevel-0.01)]],
                                                                     },
                                                                 ]
                                           }
                                   },
                               //5
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(0.5),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(0.5),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(-0.3), @(0.1), @(zLevel)], @[@(-0.03), @(0.01), @(zLevel)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(0.6),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(-0.03), @(0.01), @(zLevel)],@[@(0.1), @(-0.03- 0.02), @(zLevel)]],
                                                                     },
                                                                 ]
                                           }
                                   },
                               
                               //6
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(0),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(0.4),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeQuadrangle),
                                                                     kQuadParamKey : @[@[@(1),@(-1),@(0)], @[@(1),@(1),@(0)],@[@(- quadRation / 2),@(1),@(0)],@[@(quadRation / 2),@(-1),@(0)]],
                                                                     kPositonKey : @[@[@(-quadRation), @(2), @(zLevel + 0.01)], @[@(-quadRation * vPadding / 2), @(vPadding), @(zLevel + 0.01)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(3),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeQuadrangle),
                                                                     kQuadParamKey : @[@[@(1),@(-1),@(0)], @[@(1),@(1),@(0)],@[@(- quadRation / 2),@(1),@(0)],@[@(quadRation / 2),@(-1),@(0)]],
                                                                     kPositonKey : @[@[@(-quadRation * vPadding / 2), @(vPadding), @(zLevel + 0.005)], @[@(quadRation * vPadding / 2), @(- vPadding), @(zLevel + 0.005)]],
                                                                     },
                                                                 ]
                                           }
                                   },
                               //7
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(3),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(0.4),
                                                                     kFilterTypeKey : @(kFilterTypeChangeAlpha),
                                                                     kAnimationTypeKey : @(kAnimationTypeQuadrangle),
                                                                     kPositonKey : @[@[@(quadRation), @(-2), @(zLevel + 0.01)], @[@(quadRation * 0.03), @(-0.06), @(zLevel + 0.01)]],
                                                                     kQuadParamKey : @[@[@(quadRation / 2),@(-1),@(0)],@ [@(- quadRation / 2),@(1),@(0)], @[@(-1),@(1),@(0)], @[@(-1),@(-1),@(0)]],
                                                                     kFitlerParamKey : @[@(0), @(1)],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(3),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeQuadrangle),
                                                                     kPositonKey : @[@[@(quadRation * vPadding / 2), @(-vPadding), @(zLevel + 0.005)], @[@(-quadRation * vPadding / 2), @(vPadding), @(zLevel + 0.005)]],
                                                                     kQuadParamKey : @[@[@(quadRation / 2),@(-1),@(0)],@ [@(- quadRation / 2),@(1),@(0)], @[@(-1),@(1),@(0)], @[@(-1),@(-1),@(0)]],
                                                                     },
                                                                 ]
                                           }
                                   },
                               
                               //8
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(1.2),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(0.4),
                                                                     kFilterTypeKey : @(kFilterTypeChangeAlpha),
                                                                     kAnimationTypeKey : @(kAnimationTypeSlide),
                                                                     kPositonKey : @[@[@(0), @(0), @(zLevel + 0.01)]],
                                                                     kQuadParamKey : @[@[@(quadRation / 2),@(-1),@(0)], @[@(- quadRation / 2),@(1),@(0)],@[@(- quadRation / 2),@(1),@(0)],@[@(quadRation / 2),@(-1),@(0)], @[@(1),@(-1),@(0)], @[@(1),@(1),@(0)],@[@(-1),@(1),@(0)],@[@(-1),@(-1),@(0)]],
                                                                     kFitlerParamKey : @[@(0), @(1)],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(0.8),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(zLevel + 0.01)], @[@(0), @(0), @(zLevel)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(0.7),
                                                                     kFilterTypeKey : @(kFilterTypeChangeAlpha),
                                                                     kFitlerParamKey : @[@(1), @(0)],
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(zLevel)]],
                                                                     },
                                                                 ]
                                           }
                                   },
                               
                               //9
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(0),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(0),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(-0.3), @(0.1), @(zLevel)], @[@(-0.03), @(0.01), @(zLevel)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(0),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(-0.03), @(0.01), @(zLevel)],@[@(0.1), @(-0.03), @(zLevel)]],
                                                                     },
                                                                 ]
                                           }
                                   },
                               
                               //10
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(0),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(0.4),
                                                                     kFilterTypeKey : @(kFilterTypeChangeAlpha),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(-0.2), @(0), @(zLevel + 0.01)], @[@(0), @(0), @(zLevel + 0.01)]],
                                                                     kFitlerParamKey : @[@(0), @(1)],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(0.8),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(zLevel+0.01)],@[@(0.1), @(0), @(zLevel + 0.01)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(0.7),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0.1), @(0), @(zLevel + 0.01)],@[@(0.13), @(0), @(zLevel)]],
                                                                     },
                                                                 ]
                                           }
                                   },
                               //11
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(1.9),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(0.4),
                                                                     kFilterTypeKey : @(kFilterTypeChangeAlpha),
                                                                     kAnimationTypeKey : @(kAnimationTypeQuadrangle),
                                                                     kPositonKey : @[@[@(-0.2), @(0), @(zLevel + 0.02)]],
                                                                     kFitlerParamKey : @[@(0), @(1)],
                                                                     kQuadParamKey : @[@[@(0.4),@(-1),@(0)],@[@(0.4),@(1),@(0)], @[@(-0.4),@(1),@(0)], @[@(-0.4),@(-1),@(0)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(1.5),
                                                                     kFilterTypeKey : @(kFilterTypeChangeAlpha),
                                                                     kAnimationTypeKey : @(kAnimationTypeQuadrangle),
                                                                     kPositonKey : @[@[@(-0.2), @(0), @(zLevel+0.02)]],
                                                                     kFitlerParamKey : @[@(1), @(1)],
                                                                     kQuadParamKey : @[@[@(0.4),@(-1),@(0)],@[@(0.4),@(1),@(0)], @[@(-0.4),@(1),@(0)], @[@(-0.4),@(-1),@(0)]],
                                                                     },
                                                                 ]
                                           }
                                   },
                               
                               //12
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(2.1),
                                           kAnimaFragmentKey : @[
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(0.3),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeQuadrangle),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)], @[@(0), @(0), @(0)]],
                                                                     kQuadParamKey : @[@[@(1),@(-quadHeight / 2),@(0)],@ [@(1),@(quadHeight / 2),@(0)], @[@(-1),@(quadHeight / 2),@(0)], @[@(-1),@(-quadHeight / 2),@(0)]],
                                                                     kEnlargeKey : @[@[@(1.2), @(0.5), @(0.55)], @[@(1.2), @(0.5), @(0.45)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(1.8),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeQuadrangle),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)], @[@(0), @(0), @(0)]],
                                                                     kQuadParamKey : @[@[@(1),@(-quadHeight / 2),@(0)],@ [@(1),@(quadHeight / 2),@(0)], @[@(-1),@(quadHeight / 2),@(0)], @[@(-1),@(-quadHeight / 2),@(0)]],
                                                                     kEnlargeKey : @[@[@(1.2), @(0.5), @(0.45)], @[@(1.2), @(0.5), @(0.40)]],
                                                                     },
                                                                 ]
                                           }
                                   },
                               
                               //13
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(2.1),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(0.5),
                                                                     kFilterTypeKey : @(kFilterTypeChangeAlpha),
                                                                     kFitlerParamKey : @[@(0), @(1)],
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(-0.2), @(0), @(zLevel)], @[@(0), @(0), @(zLevel)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(1.3),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(zLevel)],@[@(0.1), @(0), @(zLevel)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(1),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0.1), @(0), @(zLevel)],@[@(0.1), @(0), @(zLevel)]],
                                                                     },
                                                                 ]
                                           }
                                   },
                               ];

    NSArray *indexArray = [NSArray array];
    
    indexArray = @[@(0), @(1), @(2), @(3), @(4), @(5), @(6), @(7), @(8), @(9), @(10), @(11), @(12)];
    
    CGFloat tempTime = 0;
    
    NSMutableArray *selectedArray = [NSMutableArray array];
    
    NSInteger theIndex = -1;
    for (NSNumber *index in indexArray) {
        theIndex++;
        //start time
        NSMutableDictionary *mutableDic = [NSMutableDictionary dictionaryWithDictionary:originalArray[index.integerValue]];
        NSMutableDictionary *animaDetailDic = [NSMutableDictionary dictionaryWithDictionary:mutableDic[kAnimaDetailKey]];
        
        [animaDetailDic setValue:@(tempTime) forKey:kAnimationStartTimeKey];
        
        //end tiem
        NSArray *animaFragmentArray = animaDetailDic[kAnimaFragmentKey];
        CGFloat tempEndTime = 0;
        for (NSDictionary *animaFragmentDic in animaFragmentArray) {
            
            NSNumber *fragmentDuration = animaFragmentDic[kAnimaFragmentDurationKey];
            tempEndTime += fragmentDuration.floatValue;
        }
        [animaDetailDic setValue:@(tempEndTime + tempTime) forKey:kAnimationEndTimeKey];
        [animaDetailDic setValue:@(theIndex) forKey:kAnimaDetailPhotoSort];
        //calc next videoFragment start time
        NSNumber *fragmentEndTime = animaDetailDic[kFragmentEndTimeKey];
        tempTime += fragmentEndTime.floatValue;
        
        
        [mutableDic setValue:animaDetailDic forKey:kAnimaDetailKey];
        [selectedArray addObject:mutableDic];
    }
    
    NSLog(@"tempTime:%f", tempTime);
    
    return selectedArray;
}

+ (NSArray *)videoFragmentArray4Sport:(CGFloat)videoEndTime
{
    CGFloat logoTime = 2.5;
    NSArray *originalArray = @[
                               //1
                               @{
                                   kAnimaDetailKey : @{
                                           kAnimationStartTimeKey : @(videoEndTime - logoTime),
                                           kAnimationEndTimeKey : @(videoEndTime),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(logoTime),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kFitlerParamKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)], @[@(0), @(0), @(0)]],
                                                                     },],
                                           }
                                   },
                               ];
    
    return originalArray;
}

+ (CGFloat)videoDuration4Sport:(NSInteger)photoCount
{
    CGFloat videoSumDuartion = 17.5;
    
    return videoSumDuartion;
}

#pragma mark - photo
+ (NSArray *)animaDetailConf4Photo:(NSInteger)photoCount
{
    CGFloat repeatRatioH = 4;
    CGFloat repeatRatioV = 4;
    CGFloat repeatRatioH2 = 4;
    CGFloat repeatRatioV2 = 4;
    CGFloat quadHeight = 0.8;
    CGPoint textureCenter = CGPointMake(0.5, 0.5);

    NSArray *originalArray = @[
                               //1
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(2.9),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(0.2),
                                                                     kFilterTypeKey : @(kFilterTypeGrayscale),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kFitlerParamKey : @[@(0), @(0)],
                                                                     kPositonKey : @[@[@(-kWidth), @(0), @(0)], @[@(0), @(0), @(0)]],
                                                                     },
                                                                 
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(0.2),
                                                                     kFilterTypeKey : @(kFilterTypeMotionBlur),
                                                                     kFitlerParamKey : @[@(0), @(5)],
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(-kHeight), @(0)], @[@(0), @(0), @(0)]],
                                                                     kEnlargeKey : @[@[@(1.3), @(textureCenter.x), @(textureCenter.y)]],
                                                                     },
                                                                 
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(0.4),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)], @[@(0), @(0), @(0)]],
                                                                     kEnlargeKey : @[@[@(1.3), @(textureCenter.x), @(textureCenter.y)], @[@(1), @(textureCenter.x), @(textureCenter.y)]],
                                                                     },
                                                                 
                                                                 
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(0.3),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)], @[@(0), @(0), @(0)]],
                                                                     kEnlargeKey : @[@[@(1), @(textureCenter.x), @(textureCenter.y)], @[@(1.1), @(textureCenter.x), @(textureCenter.y)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(1.8),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)], @[@(0), @(0), @(0)]],
                                                                     kEnlargeKey : @[@[@(1.1), @(textureCenter.x), @(textureCenter.y)], @[@(1.15), @(0.45), @(textureCenter.y)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(0.4),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)], @[@(0), @(0), @(-0.01)]],
                                                                     kEnlargeKey : @[@[@(1.15), @(0.45), @(textureCenter.y)]],
                                                                     },
                                                                 ]
                                           }
                                   },
                               //2
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(1.7),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(0.2),
                                                                     kFilterTypeKey : @(kFilterTypeGrayscale),
                                                                     kFitlerParamKey : @[@(0), @(0)],
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(-kWidth), @(0), @(0.01)], @[@(0), @(0), @(0.01)]],
                                                                     },
                                                                 
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(0.2),
                                                                     kFilterTypeKey : @(kFilterTypeMotionBlur),
                                                                     kFitlerParamKey : @[@(5), @(0)],
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(-kWidth), @(0), @(0)], @[@(0), @(0), @(0)]],
                                                                     },
                                                                 
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(0.7),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)], @[@(0), @(0), @(0)]],
                                                                     kEnlargeKey : @[@[@(1), @(textureCenter.x), @(textureCenter.y)], @[@(1.2), @(textureCenter.x), @(textureCenter.y)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(0.2),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)], @[@(0), @(0), @(0)]],
                                                                     kEnlargeKey : @[@[@(1.2), @(textureCenter.x), @(textureCenter.y)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(0.4),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeSlide),
        
                                                                     kPositonKey : @[@[@(0), @(0), @(0)], @[@(0), @(0), @(0)]],
                                                                     kQuadParamKey : @[@[@(1),@(-1),@(0)],@[@(1),@(1),@(0)], @[@(-1),@(1),@(0)], @[@(-1),@(-1),@(0)], @[@(1),@(-quadHeight / 2),@(0)],@ [@(1),@(quadHeight / 2),@(0)], @[@(-1),@(quadHeight / 2),@(0)], @[@(-1),@(-quadHeight / 2),@(0)]],
                                                                     kEnlargeKey : @[@[@(1.2), @(textureCenter.x), @(textureCenter.y)], @[@(1.1), @(textureCenter.x), @(textureCenter.y)]],
                                                                     },
                                                                 
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(0.3),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeQuadrangle),
                                                                     
                                                                     kPositonKey : @[@[@(0), @(0), @(0)], @[@(0), @(0), @(0)]],
                                                                     kQuadParamKey : @[@[@(1),@(-quadHeight / 2),@(0)],@ [@(1),@(quadHeight / 2),@(0)], @[@(-1),@(quadHeight / 2),@(0)], @[@(-1),@(-quadHeight / 2),@(0)]],
                                                                     kEnlargeKey : @[@[@(1.1), @(textureCenter.x), @(textureCenter.y)]],
                                                                     },
                                                                 ]
                                           }
                                   },
                               //3
                               @{
                                   kAnimaDetailKey : @{
                                           
                                           kFragmentEndTimeKey : @(2.2),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(0.2),
                                                                     kFilterTypeKey : @(kFilterTypeGrayscale),
                                                                     kFitlerParamKey : @[@(0), @(0)],
                                                                     kAnimationTypeKey : @(kAnimationTypeQuadrangle),
                                                                     kPositonKey : @[@[@(-kWidth), @(0), @(0)], @[@(0), @(0), @(0)]],
                                                                     kQuadParamKey : @[@[@(1),@(-quadHeight / 2),@(0)],@ [@(1),@(quadHeight / 2),@(0)], @[@(-1),@(quadHeight / 2),@(0)], @[@(-1),@(-quadHeight / 2),@(0)]],
                                                                     kEnlargeKey : @[@[@(1.3), @(textureCenter.x), @(textureCenter.y)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(0.5),
                                                                     kFilterTypeKey : @(kFilterTypeGrayscale),
                                                                     kFitlerParamKey : @[@(0), @(0)],
                                                                     kAnimationTypeKey : @(kAnimationTypeSlide),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)]],
                                                                     kQuadParamKey : @[@[@(1),@(-quadHeight / 2),@(0)],@ [@(1),@(quadHeight / 2),@(0)], @[@(-1),@(quadHeight / 2),@(0)], @[@(-1),@(-quadHeight / 2),@(0)], @[@(1),@(-1),@(0)],@[@(1),@(1),@(0)], @[@(-1),@(1),@(0)], @[@(-1),@(-1),@(0)]],
                                                                     kEnlargeKey : @[@[@(1.3), @(textureCenter.x), @(textureCenter.y)], @[@(1.25), @(0.48), @(textureCenter.y)]],
                                                                     },
                                                                 
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(0.2),
                                                                     kFilterTypeKey : @(kFilterTypeGrayscale),
                                                                     kFitlerParamKey : @[@(0), @(1)],
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)]],
                                                                     kEnlargeKey : @[@[@(1.25), @(0.48), @(textureCenter.y)], @[@(1.1), @(0.48), @(textureCenter.y)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(1.3),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0.1)], @[@(-0.06), @(0), @(0.1)]],
                                                                     kEnlargeKey : @[@[@(1.1), @(0.48), @(textureCenter.y)], @[@(1.1), @(0.55), @(textureCenter.y)]],
                                                                     },
                                                                 
                                                                 ]
                                           }
                                   },
                               //4
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(0),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(0.5),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)]],
                                                                     kEnlargeKey : @[@[@(1.2), @(textureCenter.x), @(textureCenter.y)], @[@(1.1), @(textureCenter.x), @(textureCenter.y)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(1.5),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)]],
                                                                     kEnlargeKey : @[@[@(1.1), @(textureCenter.x), @(textureCenter.y)], @[@(1.1), @(textureCenter.x), @(textureCenter.y)]],
                                                                     },
                                                                 ]
                                           }
                                   },
                               
                               //5
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(2),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(1),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kImageRepeatKey : @[@(1 / repeatRatioH), @(repeatRatioV)],
                                                                     kPositonKey : @[@[@(-(repeatRatioH - 1) / repeatRatioH), @(0), @(0.01)], @[@(-(repeatRatioH - 1) / repeatRatioH), @(0), @(0.01)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(1),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kImageRepeatKey : @[@(1 / repeatRatioH), @(repeatRatioV)],
                                                                     kPositonKey : @[@[@(-(repeatRatioH - 1) / repeatRatioH), @(0), @(0.01)], @[@(-(repeatRatioH - 1) / repeatRatioH), @(0), @(0.01)]],
                                                                     },
                                                                 ]
                                           }
                                   },
                               
                               //6
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(0),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(0.5),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)]],
                                                                     kEnlargeKey : @[@[@(1.2), @(textureCenter.x), @(textureCenter.y)], @[@(1.1), @(textureCenter.x), @(textureCenter.y)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(1),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)]],
                                                                     kEnlargeKey : @[@[@(1.1), @(textureCenter.x), @(textureCenter.y)], @[@(1.1), @(0.45), @(textureCenter.y)]],
                                                                     },
                                                                 
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(0.5),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)]],
                                                                     kEnlargeKey : @[@[@(1.1), @(0.45), @(textureCenter.y)], @[@(1.1), @(0.45), @(textureCenter.y)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(0.2),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)], @[@(kWidth / 2), @(0), @(0)]],
                                                                     kEnlargeKey : @[@[@(1.1), @(0.45), @(textureCenter.y)], @[@(1.1), @(0.45), @(textureCenter.y)]],
                                                                     },
                                                                 ]
                                           }
                                   },
                               
                               //7
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(2),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(1),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kImageRepeatKey : @[@(repeatRatioH2), @(1 / repeatRatioV2)],
                                                                     kPositonKey : @[@[@(0), @(-(repeatRatioV2 - 0.7) / repeatRatioV2), @(0.01)], @[@(0), @(-(repeatRatioV2 - 0.7) / repeatRatioV2), @(0.01)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(1),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kImageRepeatKey : @[@(repeatRatioH2), @(1 / repeatRatioV2)],
                                                                     kPositonKey : @[@[@(0), @(-(repeatRatioV2 - 0.7) / repeatRatioV2), @(0.01)], @[@(0), @(-(repeatRatioV2 - 0.7) / repeatRatioV2), @(0)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(0.2),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kImageRepeatKey : @[@(repeatRatioH2), @(1 / repeatRatioV2)],
                                                                     kPositonKey : @[@[@(0), @(-(repeatRatioV2 - 0.7) / repeatRatioV2), @(0)], @[@(kWidth / 2), @(-(repeatRatioV2 - 0.7) / repeatRatioV2), @(0)]],
                                                                     },
                                                                 ]
                                           }
                                   },
                               
                               //8
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(1.1),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(0.2),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeSlide),
                                                                     kPositonKey : @[@[@(-kWidth), @(0), @(0)], @[@(0), @(0), @(0)]],
                                                                     kFitlerParamKey : @[@(5), @(0)],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(0.9),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)], @[@(0), @(0), @(0)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(0.2),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)], @[@(kWidth / 2), @(0), @(0)]],
                                                                     },
                                                                 ]
                                           }
                                   },
                               
                               //9
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(1.3),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(0.2),
                                                                     kFilterTypeKey : @(kFilterTypeMotionBlur),
                                                                     kFitlerParamKey : @[@(5), @(0)],
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(-kWidth), @(0), @(0)], @[@(0), @(0), @(0)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(1.1),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(0.2),
                                                                     kFilterTypeKey : @(kFilterTypeMotionBlur),
                                                                     kFitlerParamKey : @[@(5), @(0)],
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)], @[@(kWidth / 2), @(0), @(0)]],
                                                                     },
                                                                 ]
                                           }
                                   },
                               
                               //10
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(1.3),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(0.2),
                                                                     kFilterTypeKey : @(kFilterTypeMotionBlur),
                                                                     kFitlerParamKey : @[@(5), @(0)],
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(-kWidth), @(0), @(0)], @[@(0), @(0), @(0)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(1.1),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)]],
                                                                     },
                                                                 ]
                                           }
                                   },
                               
                               ];
    
    NSArray *indexArray = [NSArray array];
    
    indexArray = @[@(0), @(1), @(2), @(3), @(4), @(5), @(6), @(7), @(8), @(9)];
    
    CGFloat tempTime = 0;
    
    NSMutableArray *selectedArray = [NSMutableArray array];
    
    NSInteger theIndex = -1;
    for (NSNumber *index in indexArray) {
        theIndex++;
        //start time
        NSMutableDictionary *mutableDic = [NSMutableDictionary dictionaryWithDictionary:originalArray[index.integerValue]];
        NSMutableDictionary *animaDetailDic = [NSMutableDictionary dictionaryWithDictionary:mutableDic[kAnimaDetailKey]];
        
        [animaDetailDic setValue:@(tempTime) forKey:kAnimationStartTimeKey];
        
        //end tiem
        NSArray *animaFragmentArray = animaDetailDic[kAnimaFragmentKey];
        CGFloat tempEndTime = 0;
        for (NSDictionary *animaFragmentDic in animaFragmentArray) {
            
            NSNumber *fragmentDuration = animaFragmentDic[kAnimaFragmentDurationKey];
            tempEndTime += fragmentDuration.floatValue;
        }
        [animaDetailDic setValue:@(tempEndTime + tempTime) forKey:kAnimationEndTimeKey];
        [animaDetailDic setValue:@(theIndex) forKey:kAnimaDetailPhotoSort];
        //calc next videoFragment start time
        NSNumber *fragmentEndTime = animaDetailDic[kFragmentEndTimeKey];
        tempTime += fragmentEndTime.floatValue;
        
        
        [mutableDic setValue:animaDetailDic forKey:kAnimaDetailKey];
        [selectedArray addObject:mutableDic];
    }
    
    return selectedArray;
}

+ (NSArray *)videoFragmentArray4Photo:(CGFloat)videoEndTime
{
    CGFloat logoTime = 2.5;
    CGFloat Interval = 1.5;
    CGFloat focusDuration = 0.5;
    NSArray *originalArray = @[
                               //1
                               @{
                                   kAnimaDetailKey : @{
                                           kAnimationStartTimeKey : @(0),
                                           kAnimationEndTimeKey : @(videoEndTime - logoTime - Interval),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(videoEndTime - logoTime - Interval),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)], @[@(0), @(0), @(0)]],
                                                                     kFitlerParamKey : @[@(0.15)],

                                                                     }],
                                           }
                                   },
                               //2
                               @{
                                   kAnimaDetailKey : @{
                                           kAnimationStartTimeKey : @(0.6),
                                           kAnimationEndTimeKey : @(0.6 + focusDuration),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(focusDuration),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)], @[@(0), @(0), @(0)]],
                                                                     kFitlerParamKey : @[@(0.15)],

                                                                     }],
                                           }
                                   },
                               //3
                               @{
                                   kAnimaDetailKey : @{
                                           kAnimationStartTimeKey : @(3.5),
                                           kAnimationEndTimeKey : @(3.5 + focusDuration),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(focusDuration),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)], @[@(0), @(0), @(0)]],
                                                                     kFitlerParamKey : @[@(0.15)],
                                                                     }],
                                           }
                                   },
                               //4
                               @{
                                   kAnimaDetailKey : @{
                                           kAnimationStartTimeKey : @(5.0),
                                           kAnimationEndTimeKey : @(5.0 + focusDuration),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(focusDuration),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)], @[@(0), @(0), @(0)]],
                                                                     kFitlerParamKey : @[@(0.15)],
                                                                     }],
                                           }
                                   },
                               //5
                               @{
                                   kAnimaDetailKey : @{
                                           kAnimationStartTimeKey : @(7.2),
                                           kAnimationEndTimeKey : @(7.2 + focusDuration),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(focusDuration),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)], @[@(0), @(0), @(0)]],
                                                                     kFitlerParamKey : @[@(0.15)],
                                                                     }],
                                           }
                                   },
                               //6
                               @{
                                   kAnimaDetailKey : @{
                                           kAnimationStartTimeKey : @(10.3),
                                           kAnimationEndTimeKey : @(10.3 + focusDuration),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(focusDuration),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)], @[@(0), @(0), @(0)]],
                                                                     kFitlerParamKey : @[@(0.15)],
                                                                     }],
                                           }
                                   },
                               //7
                               @{
                                   kAnimaDetailKey : @{
                                           kAnimationStartTimeKey : @(11.2),
                                           kAnimationEndTimeKey : @(11.2 + focusDuration),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(focusDuration),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)], @[@(0), @(0), @(0)]],
                                                                     kFitlerParamKey : @[@(0.15)],
                                                                     }],
                                           }
                                   },
                               //8
                               @{
                                   kAnimaDetailKey : @{
                                           kAnimationStartTimeKey : @(12.4),
                                           kAnimationEndTimeKey : @(12.4 + focusDuration),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(focusDuration),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)], @[@(0), @(0), @(0)]],
                                                                     kFitlerParamKey : @[@(0.15)],
                                                                     }],
                                           }
                                   },
                               //9
                               @{
                                   kAnimaDetailKey : @{
                                           kAnimationStartTimeKey : @(13.7),
                                           kAnimationEndTimeKey : @(13.7 + focusDuration),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(focusDuration),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)], @[@(0), @(0), @(0)]],
                                                                     kFitlerParamKey : @[@(0.15)],
                                                                     }],
                                           }
                                   },
                               //10
                               @{
                                   kAnimaDetailKey : @{
                                           kAnimationStartTimeKey : @(videoEndTime - logoTime),
                                           kAnimationEndTimeKey : @(videoEndTime),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(logoTime),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeNone),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)], @[@(0), @(0), @(0)]],
                                                                     kFitlerParamKey : @[@(0.15)],
                                                                     },
                                                                 ],
                                           }
                                   },
                               ];
    
    return originalArray;
}

+ (CGFloat)videoDuration4Photo:(NSInteger)photoCount
{
    CGFloat videoSumDuartion = 17;
    
    return videoSumDuartion;
}

#pragma mark - curlpage
+ (NSArray *)animaDetailConf4CurlPage:(NSInteger)photoCount
{
    
    CGFloat flipDuration = 2;
    CGFloat duration = 3.5;
    
    NSArray *originalArray = @[
                               @{
                                   //1
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(duration),
                                           kAnimaDetailSort : @(4),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(duration),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeCurl),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(flipDuration),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeCurl),
                                                                     kAmimationParamKey : @[@[@(-50), @(50)], @(M_PI_2 + 0.4), @(50)],
                                                                     kPositonKey : @[@[@(0), @(0), @(0)]],
                                                                     },
                                                                 ]
                                           }
                                   },
                               //2
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(duration),
                                           kAnimaDetailSort : @(3),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(duration),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeCurl),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(flipDuration),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeCurl),
                                                                     kAmimationParamKey : @[@[@(-50), @(-50)], @(M_PI_2 + 0.4), @(50)],
                                                                     kPositonKey : @[@[@(0), @(0), @(0)]],
                                                                     },
                                                                 ]
                                           }
                                   },
                               
                               //3
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(duration),
                                           kAnimaDetailSort : @(2),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(duration),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeCurl),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(flipDuration),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeCurl),
                                                                     kAmimationParamKey : @[@[@(-50), @(-50)], @(M_PI_2 + 0.4), @(50)],
                                                                     kPositonKey : @[@[@(0), @(0), @(0)]],
                                                                     },
                                                                 ]
                                           }
                                   },
                               //4
                               @{
                                   kAnimaDetailKey : @{
                                           kFragmentEndTimeKey : @(duration),
                                           kAnimaDetailSort : @(1),
                                           kAnimaFragmentKey : @[@{
                                                                     kAnimaFragmentDurationKey : @(duration),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeCurl),
                                                                     kPositonKey : @[@[@(0), @(0), @(0)]],
                                                                     },
                                                                 @{
                                                                     kAnimaFragmentDurationKey : @(flipDuration),
                                                                     kFilterTypeKey : @(kFilterTypeNone),
                                                                     kAnimationTypeKey : @(kAnimationTypeCurl),
                                                                     kAmimationParamKey : @[@[@(-50), @(-50)], @(M_PI_2 + 0.4), @(50)],
                                                                     kPositonKey : @[@[@(0), @(0), @(0)]],
                                                                     },
                                                                 ]
                                           }
                                   },
                               ];
    
    NSArray *indexArray = [NSArray array];
    indexArray = @[@(0), @(1), @(2), @(3)];
    
    CGFloat tempTime = 0;
    
    NSMutableArray *selectedArray = [NSMutableArray array];
    
    int theIndex = -1;
    for (NSNumber *index in indexArray) {
        
        theIndex++;
        //start time
        NSMutableDictionary *mutableDic = [NSMutableDictionary dictionaryWithDictionary:originalArray[index.integerValue]];
        NSMutableDictionary *animaDetailDic = [NSMutableDictionary dictionaryWithDictionary:mutableDic[kAnimaDetailKey]];
        
        [animaDetailDic setValue:@(tempTime) forKey:kAnimationStartTimeKey];
        
        //end tiem
        NSArray *animaFragmentArray = animaDetailDic[kAnimaFragmentKey];
        CGFloat tempEndTime = 0;
        for (NSDictionary *animaFragmentDic in animaFragmentArray) {
            
            NSNumber *fragmentDuration = animaFragmentDic[kAnimaFragmentDurationKey];
            tempEndTime += fragmentDuration.floatValue;
        }
        [animaDetailDic setValue:@(tempEndTime + tempTime) forKey:kAnimationEndTimeKey];
        [animaDetailDic setValue:@(theIndex) forKey:kAnimaDetailPhotoSort];
        
        //calc next videoFragment start time
        NSNumber *fragmentEndTime = animaDetailDic[kFragmentEndTimeKey];
        tempTime += fragmentEndTime.floatValue;
        
        [mutableDic setValue:animaDetailDic forKey:kAnimaDetailKey];
        [mutableDic setValue:animaDetailDic[kAnimaDetailSort] forKey:kAnimaDetailSort];
        [selectedArray addObject:mutableDic];
    }
    
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:kAnimaDetailSort ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:descriptor];
    NSArray *sortArray = [selectedArray sortedArrayUsingDescriptors:sortDescriptors];
    selectedArray = [NSMutableArray arrayWithArray:sortArray];
    
    return selectedArray;
}

+ (NSArray *)videoFragmentArray4CurlPage:(CGFloat)videoEndTime
{
    return nil;
}

+ (CGFloat)videoDuration4CurlPage:(NSInteger)photoCount
{
    return 10;
}
@end
