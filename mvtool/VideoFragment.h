//
//  VideoFragment.h
//  mvtool
//
//  Created by rumble on 14-6-6.
//  Copyright (c) 2014年 PixShow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OpenGLView.h"
#import "AnimaDetail.h"

@interface VideoFragment : NSObject

@property(nonatomic) TextureInfo     textureInfo;
@property(nonatomic) GLuint          vertexBuffer;
@property(nonatomic) GLuint          indexBuffer;
@property(nonatomic) AnimaDetail     *animaDetail;
@property(nonatomic) id              imageRepeatParam;
@property(nonatomic) NSInteger       photoIndex;
@end
