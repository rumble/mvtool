//
//  VideoOverlayFragment.h
//  mvtool
//
//  Created by rumble on 14/6/9.
//  Copyright (c) 2014年 PixShow. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "OpenGLView.h"

@class VideoFragment;
@interface VideoOverlayFragment : NSObject

@property (nonatomic, strong) AVAssetReaderTrackOutput *mOutput;
@property (nonatomic, strong) AVAssetReader *mReader;
@property (nonatomic, strong) VideoFragment *videoFragment;
@property (nonatomic) ShaderParams  shaderParams;
@property (nonatomic) BOOL  finished;
@end
