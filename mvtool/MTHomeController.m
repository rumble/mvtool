//
//  MTHomeController.m
//  mvtool
//
//  Created by rumble on 14-4-23.
//  Copyright (c) 2014年 PixShow. All rights reserved.
//

#import "MTHomeController.h"
#import "OpenGLView.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "ELCImagePickerController.h"
#import "ELCAlbumPickerController.h"
#import "ELCAssetTablePicker.h"

@interface MTHomeController ()<ELCImagePickerControllerDelegate, UINavigationControllerDelegate, UIScrollViewDelegate> 

@property (nonatomic) OpenGLView    *openGLView;
@property (nonatomic) UIButton      *pickerButton;
@property (nonatomic) UIButton      *stopButton;
@property (nonatomic) UIButton      *startButton;
@property (nonatomic) UISegmentedControl    *mvTypeSegmentedControl;
@end

@implementation MTHomeController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view addSubview:self.openGLView];
    //[self.view addSubview:self.pickerButton];
    [self.view addSubview:self.mvTypeSegmentedControl];
    
    CGRect frame = _mvTypeSegmentedControl.frame;
    
    frame.origin = CGPointMake(10, 340);
    _mvTypeSegmentedControl.frame = frame;
    
//    CGRect frame = _pickerButton.frame;
//    
//    frame.origin = CGPointMake(10, 340);
//    _pickerButton.frame = frame;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (OpenGLView *)openGLView
{
    if (!_openGLView) {
        
        self.openGLView = [[OpenGLView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.width)];
    }
    
    return _openGLView;
}

- (UIButton *)pickerButton
{
    if (!_pickerButton) {
        
        self.pickerButton = [UIButton new];
        [_pickerButton addTarget:self action:@selector(pickerClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_pickerButton setTitle:@"选择图片" forState:UIControlStateNormal];
        [_pickerButton sizeToFit];
        [_pickerButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    
    return _pickerButton;
}

- (UISegmentedControl *)mvTypeSegmentedControl
{
    if (!_mvTypeSegmentedControl) {
        
        self.mvTypeSegmentedControl = [[UISegmentedControl alloc] initWithItems:@[@"回忆",@"运动",@"拍照", @"翻页"]];
        _mvTypeSegmentedControl.selectedSegmentIndex = 3;
        [_mvTypeSegmentedControl addTarget:self action:@selector(segmentedControlChanged:) forControlEvents:UIControlEventValueChanged];
    }
    
    return _mvTypeSegmentedControl;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - private
- (void)pickerClicked:(id)sender
{
    [_openGLView stopPlay];
    
    ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] initImagePicker];
    elcPicker.maximumImagesCount = 12;
    elcPicker.returnsOriginalImage = NO; //Only return the fullScreenImage, not the fullResolutionImage
    elcPicker.imagePickerDelegate = self;
    
    [self presentViewController:elcPicker animated:YES completion:nil];
}

- (void)setupImages:(NSArray *)info
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSMutableArray *images = [NSMutableArray arrayWithCapacity:[info count]];
        
        if (info.count > 12 || info.count < 5) {

            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"只能5-9张图" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
            
            [alertView show];
        }else {
            for (NSDictionary *dict in info) {
                
                UIImage *image = [dict objectForKey:UIImagePickerControllerOriginalImage];
                [images addObject:image];
            }
            
            [_openGLView setDisplayImages:images];
            [_openGLView startPlay];
        }
    });
}

- (void)segmentedControlChanged:(UISegmentedControl *)segmentedControl
{
    [_openGLView playDemoWithType:segmentedControl.selectedSegmentIndex];
}

#pragma mark ELCImagePickerControllerDelegate Methods

- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
{
    [self dismissViewControllerAnimated:YES completion:nil];

    [self performSelector:@selector(setupImages:) withObject:info afterDelay:0.2];
}

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [_openGLView startPlay];
}
@end
