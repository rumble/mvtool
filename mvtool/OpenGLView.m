//
//  OpenGLView.m
//  HelloOpenGL
//
//  Created by Ray Wenderlich on 5/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "OpenGLView.h"
#import "CC3GLMatrix.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "VideoFragment.h"
#import "AnimaDetail.h"
#import "AnimaFragment.h"
#import "VideoModel.h"
#import "VideoOverlayFragment.h"
#import <Accelerate/Accelerate.h>
#import "MTUtility.h"

extern NSString *kImageKey;
extern NSString *kPositonKey;
extern NSString *kEnlargeKey;
extern NSString *kImageRepeatKey;
extern NSString *kAnimaDetailKey;

extern NSString *kAnimaFragmentKey;
extern NSString *kAnimationStartTimeKey;
extern NSString *kAnimationEndTimeKey;
extern NSString *kAnimaFragmentDurationKey;
extern NSString *kFilterTypeKey;
extern NSString *kFitlerParamKey;
extern NSString *kQuadParamKey;
extern NSString *kAmimationParamKey;
extern NSString *kAnimationTypeKey;
extern NSString *kAnimaDetailPhotoSort;

static NSArray *kVideoNameArray;

static NSInteger kPhotoNum;
static NSInteger kPhotoMAXNum = 15;
static NSInteger kOverlayMAXNum = 10;
static NSString *kAudioSound;

typedef struct {
    float Position[3];
    float Color[4];
}VertexAndColor;

typedef struct _Vertex4Curl
{
    GLfloat x, y, z;
    GLfloat u, v;
} Vertex4Curl;

@interface OpenGLView ()
{
    Vertex4Curl *curlVertices;
    GLushort *curlIndices;
    TextureInfo backTextureInfo;
}

@property (nonatomic, assign) CGPoint cylinderPosition;
@property (nonatomic, assign) CGFloat cylinderAngle;
@property (nonatomic, assign) CGFloat cylinderRadius;
@end

@implementation OpenGLView

typedef struct {
    float Position[3];
    float TexCoord[2];
} Vertex;

static const GLfloat rotate180TextureCoordinates[] = {
    1.0f, 1.0f,
    0.0f, 1.0f,
    1.0f, 0.0f,
    0.0f, 0.0f,
};


const Vertex Vertices[] = {
#if 1
    {{1, -1, 0}, {1, 1}},
    {{1, 1, 0}, {1, 0}},
    {{-1, 1, 0}, {0, 0}},
    {{-1, -1, 0}, {0, 1}},
#else
    {{-1, -1, 0}, {0, 0}},
    {{1, -1, 0}, {1, 0}},
    {{-1, 1, 0}, {0, 1}},
    {{1, 1, 0}, {1, 1}},
#endif
};

const VertexAndColor verticesAndColors[] = {
    {{1, -1, 0}, {1, 1, 1, 1}},
    {{1, 1, 0}, {1, 1, 1, 1}},
    {{-1, 1, 0}, {1, 1, 1, 1}},
    {{-1, -1, 0}, {1, 1, 1, 1}},
};

void OrthoM4x4(GLfloat *out, GLfloat left, GLfloat right, GLfloat bottom, GLfloat top, GLfloat near, GLfloat far)
{
    out[0] = 2.f/(right-left); out[4] = 0.f; out[8] = 0.f; out[12] = -(right+left)/(right-left);
    out[1] = 0.f; out[5] = 2.f/(top-bottom); out[9] = 0.f; out[13] = -(top+bottom)/(top-bottom);
    out[2] = 0.f; out[6] = 0.f; out[10] = -2.f/(far-near); out[14] = -(far+near)/(far-near);
    out[3] = 0.f; out[7] = 0.f; out[11] = 0.f; out[15] = 1.f;
}

void Vertices4Expand(Vertex vertices[], float *percent)
{
    for (int i = 0; i < sizeof(vertices); i++) {
        
        Vertex *vertex = &vertices[i];
        float *position = vertex->Position;
        float *texCoord = vertex->TexCoord;
        
        float hpercent = percent[0];
        float vpercent = percent[1];
        
        position[0] = position[0] * hpercent;
        position[1] = position[1] * vpercent;
    }
}

void Vertices4CustomQuad(Vertex vertices[],  CC3Vector *vectors)
{
    
    Vertex *vertex1 = &vertices[0];
    Vertex *vertex3 = &vertices[2];
    
    float centerX = (vertex1->Position[0] + vertex3->Position[0]) / 2;
    float centerY = (vertex1->Position[1] + vertex3->Position[1]) / 2;
    
    float width = fabs(vertex1->Position[0] - vertex3->Position[0]);
    float height = fabs(vertex1->Position[1] - vertex3->Position[1]);
    
//    float texcenterX = (vertex1->TexCoord[0] + vertex3->TexCoord[0]) / 2;
//    float texcenterY = (vertex1->TexCoord[1] + vertex3->TexCoord[1]) / 2;
    
    float texwidth = fabs(vertex1->TexCoord[0] - vertex3->TexCoord[0]);
    float texheight = fabs(vertex1->TexCoord[1] - vertex3->TexCoord[1]);
    
    
    for (int i = 0; i < sizeof(vertices); i++) {
        
        Vertex *vertex = &vertices[i];
        float *position = vertex->Position;
        float *texCoord = vertex->TexCoord;

        CC3Vector vector = vectors[i];
        
        float texCoordX = (vector.x - centerX + width / 2) / 2;
        float texCoordY = (centerY + height / 2 - vector.y) / 2;

        position[0] = vector.x * width / 2 + centerX;
        position[1] = vector.y * height / 2 + centerY;
        
        texCoord[0] = texwidth * texCoordX + (1 - texwidth) / 2;
        texCoord[1] = texheight * texCoordY + (1 - texheight) / 2;
    }
}


void Vertices4Enlarge(Vertex vertices[], float ratio, CGPoint center)
{
    for (int i = 0; i < sizeof(vertices); i++) {
        
        Vertex *vertex = &vertices[i];
        float *position = vertex->Position;
        float *texCoord = vertex->TexCoord;
        
        texCoord[0] = (texCoord[0] - 0.5) * 1 / ratio + center.x;
        texCoord[1] = (texCoord[1] - 0.5) * 1 / ratio + center.y;
    }
}

void Vertices4Repeat(Vertex vertices[], float *ratio)
{
    for (int i = 0; i < sizeof(vertices); i++) {

        Vertex *vertex = &vertices[i];
        float *position = vertex->Position;
        float *texCoord = vertex->TexCoord;
        
        float hpercent = ratio[0];
        float vpercent = ratio[1];
        
        if (hpercent > 1) {
            
            texCoord[0] = (texCoord[0] - 0.5) * hpercent + hpercent / 2;
        }else {
            position[0] = position[0] * hpercent;
        }
        
        if (vpercent > 1) {
            texCoord[1] = (texCoord[1] - 0.5) * vpercent + vpercent / 2;
        }else {
            position[1] = position[1] * vpercent;
        }
    }
}

const GLubyte Indices[] = {
    0, 1, 2,
    2, 3, 0,
};

+ (Class)layerClass {
    return [CAEAGLLayer class];
}

- (void)setupLayer
{
    _eaglLayer = (CAEAGLLayer*) self.layer;
    _eaglLayer.opaque = YES;
    _eaglLayer.drawableProperties = @{
                                     kEAGLDrawablePropertyRetainedBacking: [NSNumber numberWithBool:YES],
                                     kEAGLDrawablePropertyColorFormat: kEAGLColorFormatRGBA8
                                     };
}

- (CAEAGLLayer *)eaglLayer
{
    return _eaglLayer;
}

- (void)setupContext
{
    EAGLRenderingAPI api = kEAGLRenderingAPIOpenGLES2;
    _context = [[EAGLContext alloc] initWithAPI:api];
    if (!_context) {
        NSLog(@"Failed to initialize OpenGLES 2.0 context");
        exit(1);
    }
    
    if (![EAGLContext setCurrentContext:_context]) {
        NSLog(@"Failed to set current OpenGL context");
        exit(1);
    }
}

- (void)setupRenderBuffer
{
    glGenRenderbuffers(1, &_colorRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);
    [_context renderbufferStorage:GL_RENDERBUFFER fromDrawable:_eaglLayer];
}

- (void)setupDepthBuffer
{
    glGenRenderbuffers(1, &_depthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _depthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, self.frame.size.width, self.frame.size.height);
}

- (void)setupFrameBuffer
{
    glGenFramebuffers(1, &_framebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, _framebuffer);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, _colorRenderBuffer);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, _depthRenderBuffer);
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &viewportWidth);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &viewportHeight);
    //Create multisampling buffers
    if (self.antialiasing) {
        
        glGenFramebuffers(1, &_sampleFramebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, _sampleFramebuffer);
        
        glGenRenderbuffers(1, &_sampleColorRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, _sampleColorRenderbuffer);
        glRenderbufferStorageMultisampleAPPLE(GL_RENDERBUFFER, 4, GL_RGBA8_OES,viewportWidth, viewportHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, _sampleColorRenderbuffer);
        
        GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
        
        if (status != GL_FRAMEBUFFER_COMPLETE) {
            NSLog(@"Failed to create multisamping framebuffer: 0x%X", status);
        }
        
        glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);
    }
}

- (GLuint)compileShader:(NSString*)shaderName withType:(GLenum)shaderType
{
    // 1
    NSString* shaderPath = [[NSBundle mainBundle] pathForResource:shaderName ofType:@"glsl"];
    NSError* error;
    NSString* shaderString = [NSString stringWithContentsOfFile:shaderPath encoding:NSUTF8StringEncoding error:&error];
    if (!shaderString) {
        NSLog(@"Error loading shader: %@", error.localizedDescription);
        exit(1);
    }
    
    // 2
    GLuint shaderHandle = glCreateShader(shaderType);    
    
    // 3
    const char * shaderStringUTF8 = [shaderString UTF8String];    
    int shaderStringLength = (int)[shaderString length];
    glShaderSource(shaderHandle, 1, &shaderStringUTF8, &shaderStringLength);
    
    // 4
    glCompileShader(shaderHandle);
    
    // 5
    GLint compileSuccess;
    glGetShaderiv(shaderHandle, GL_COMPILE_STATUS, &compileSuccess);
    if (compileSuccess == GL_FALSE) {
        GLchar messages[256];
        glGetShaderInfoLog(shaderHandle, sizeof(messages), 0, &messages[0]);
        NSString *messageString = [NSString stringWithUTF8String:messages];
        NSLog(@"%@", messageString);
        exit(1);
    }
    
    return shaderHandle;
}

- (ShaderParams)compileShadersWithVertex:(NSString *)vertexString fragment:(NSString *)fragmentString
{
    ShaderParams shaderParams;
    
    // 1
    GLuint vertexShader = [self compileShader:vertexString withType:GL_VERTEX_SHADER];
    GLuint fragmentShader = [self compileShader:fragmentString withType:GL_FRAGMENT_SHADER];
    
    // 2
    GLuint programHandle = glCreateProgram();
    glAttachShader(programHandle, vertexShader);
    glAttachShader(programHandle, fragmentShader);
    glLinkProgram(programHandle);
    
    // 3
    GLint linkSuccess;
    glGetProgramiv(programHandle, GL_LINK_STATUS, &linkSuccess);
    if (linkSuccess == GL_FALSE) {
        GLchar messages[256];
        glGetProgramInfoLog(programHandle, sizeof(messages), 0, &messages[0]);
        NSString *messageString = [NSString stringWithUTF8String:messages];
        NSLog(@"%@", messageString);
        exit(1);
    }
    
    glUseProgram(programHandle);
    
    //Attribs
    GLuint positionAttrib = glGetAttribLocation(programHandle, "Position");
    glEnableVertexAttribArray(positionAttrib);
    shaderParams.attribs[ATTRIBS_VERTEX] = positionAttrib;
    
    if ([vertexString isEqualToString:@"ColorSimpleVertex"]) {
        GLuint sourceColorAttrib = glGetAttribLocation(programHandle, "SourceColor");
        glEnableVertexAttribArray(sourceColorAttrib);
        shaderParams.attribs[ATTRIBS_SOURCECOLOR] = sourceColorAttrib;
    }else {
        GLuint texCoordAttrib = glGetAttribLocation(programHandle, "TexCoordIn");
        glEnableVertexAttribArray(texCoordAttrib);
        shaderParams.attribs[ATTRIBS_TEXCOORD] = texCoordAttrib;
    }
    
    //Uniforms
    GLuint projectionUniform = glGetUniformLocation(programHandle, "Projection");
    GLuint modelViewUniform = glGetUniformLocation(programHandle, "Modelview");
    GLuint textureUniform = glGetUniformLocation(programHandle, "Texture");
    GLuint textureUniform2 = glGetUniformLocation(programHandle, "Texture2");
    GLuint alphaUniform = glGetUniformLocation(programHandle, "alpha");
    GLuint motionBlurUniform = glGetUniformLocation(programHandle, "directionalTexelStep");
    GLuint exposureUniform = glGetUniformLocation(programHandle, "exposure");
    GLuint saturationUniform = glGetUniformLocation(programHandle, "saturation");
    //Assign
    shaderParams.programHandle = programHandle;

    shaderParams.uniforms[UNIFORM_PROJECTION] = projectionUniform;
    shaderParams.uniforms[UNIFORM_MODELVIEW] = modelViewUniform;
    
    shaderParams.uniforms[UNIFORM_TEXTURE] = textureUniform;
    shaderParams.uniforms[UNIFORM_TEXTURE1] = textureUniform2;
    
    shaderParams.uniforms[UNIFORM_ALPHA] = alphaUniform;
    
    shaderParams.uniforms[UNIFORM_MOTION_BLUR_STEP] = motionBlurUniform;
    shaderParams.uniforms[UNIFORM_EXPOSURE] = exposureUniform;
    shaderParams.uniforms[UNIFORM_SATURATION] = saturationUniform;
    
    if ([vertexString isEqualToString:@"FrontVertexShader"] || [vertexString isEqualToString:@"BackVertexShader"]) {
        
        GLuint cylinderPositionUniform = glGetUniformLocation(programHandle, "u_cylinderPosition");
        GLuint cylinderDirectionUniform = glGetUniformLocation(programHandle, "u_cylinderDirection");
        GLuint cylinderRadiusUniform = glGetUniformLocation(programHandle, "u_cylinderRadius");
        
        shaderParams.uniforms[UNIFORM_CYLINDERPOSITION] = cylinderPositionUniform;
        shaderParams.uniforms[UNIFORM_CYLINDERDIRECTION] = cylinderDirectionUniform;
        shaderParams.uniforms[UNIFORM_CYLINDERRADIUS] = cylinderRadiusUniform;
    }
    
    if ([vertexString isEqualToString:@"BackVertexShader"]) {
        
        GLuint backGradientUniform = glGetUniformLocation(programHandle, "s_gradient");
        shaderParams.uniforms[UNIFORM_BACKGRADIENT] = backGradientUniform;
    }
    
    return shaderParams;
}

- (void)setupVBOs
{
    for (VideoFragment *videoFragment in _videoFragmentArray) {
        
        GLuint vertexBuffer;
        glGenBuffers(1, &vertexBuffer);
        glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
        glBufferData(GL_ARRAY_BUFFER, sizeof(Vertices), Vertices, GL_STATIC_DRAW);
        videoFragment.vertexBuffer = vertexBuffer;
        
        GLuint indexBuffer;
        glGenBuffers(1, &indexBuffer);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Indices), Indices, GL_STATIC_DRAW);
        videoFragment.indexBuffer = indexBuffer;
    }
    
    
    for (VideoOverlayFragment *videoOverlayFragment in _overlayVideosArray) {
        
        VideoFragment *videoFragment = videoOverlayFragment.videoFragment;
        GLuint vertexBuffer;
        glGenBuffers(1, &vertexBuffer);
        glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
        glBufferData(GL_ARRAY_BUFFER, sizeof(Vertices), Vertices, GL_STATIC_DRAW);
        videoFragment.vertexBuffer = vertexBuffer;
        
        GLuint indexBuffer;
        glGenBuffers(1, &indexBuffer);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Indices), Indices, GL_STATIC_DRAW);
        videoFragment.indexBuffer = indexBuffer;
    }
}

- (void)setupDisplayLink {
    
    self.displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(render2:)];
    _displayLink.frameInterval = 2;
    [_displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
}

void SetupTextureWithImage(UIImage *sourceImage, TextureInfo *textureInfo, BOOL isNormal)
{
    CGImageRef imageRef = sourceImage.CGImage;
    size_t width = CGImageGetWidth(imageRef);
    size_t height = CGImageGetHeight(imageRef);
    
    GLubyte * sourceData = (GLubyte *) calloc(width * height * 4, sizeof(GLubyte));
    CGContextRef spriteContext = CGBitmapContextCreate(sourceData, width, height, 8, width *  4, CGImageGetColorSpace(imageRef), (CGBitmapInfo)kCGImageAlphaPremultipliedLast);
    CGContextDrawImage(spriteContext, CGRectMake(0, 0, width, height), imageRef);
    
    CGContextRelease(spriteContext);
    
    glBindTexture(GL_TEXTURE_2D, textureInfo->textureId);
    
    if (isNormal) {
        
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        
    }else {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }
    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (GLsizei)width, (GLsizei)height, 0, GL_RGBA, GL_UNSIGNED_BYTE, sourceData);
    
    free(sourceData);
    
    textureInfo->size = CGSizeMake(width, height);
}

- (TextureInfo)setupTexture:(NSString *)fileName fillMode:(BOOL)isFill {
    
    // load image
    UIImage *sourceImage = [UIImage imageNamed:fileName];
    if (!sourceImage) {
        NSLog(@"Failed to load image %@", fileName);
        exit(1);
    }
    
    TextureInfo textureInfo;
    GLuint texName;
    glGenTextures(1, &texName);
    glBindTexture(GL_TEXTURE_2D, texName);
    
    SetupTextureWithImage(sourceImage, &textureInfo, isFill);
    
    return textureInfo;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _antialiasing = YES;
        
        [self setupLayer];
        [self setupContext];
        [self setupDepthBuffer];
        [self setupRenderBuffer];
        [self setupFrameBuffer];
        
        self.mvType = KMVTypeCurlPage;
        
        simpleShaderParams = [self compileShadersWithVertex:@"SimpleVertex" fragment:@"SimpleFragment"];
        motionBlurShaderParmas = [self compileShadersWithVertex:@"MotionBlurVertex" fragment:@"MotionBlurFragment"];
        exposureShaderParmas = [self compileShadersWithVertex:@"SimpleVertex" fragment:@"ExposureFragment"];
        colorShaderParmas = [self compileShadersWithVertex:@"ColorSimpleVertex" fragment:@"ColorSimpleFragment"];
        grayscaleParmas = [self compileShadersWithVertex:@"SimpleVertex" fragment:@"GrayscaleFragment"];
        frontVertexParmas = [self compileShadersWithVertex:@"FrontVertexShader" fragment:@"FrontFragmentShader"];
        backVertexParmas = [self compileShadersWithVertex:@"BackVertexShader" fragment:@"BackFragmentShader"];
        
        backTextureInfo = [self setupTexture:@"BackPageGradient.png" fillMode:YES];
        GLuint xRes = viewportWidth / 10;
        GLuint yRes = viewportHeight / 10;
        GLsizeiptr verticesSize = (xRes+1)*(yRes+1)*sizeof(Vertex4Curl);
        self.cylinderPosition = CGPointMake(viewportWidth, viewportHeight);
        self.cylinderAngle = M_PI_2;
        self.cylinderRadius = 50;
        
        NSLog(@"viewportWidth:%d,%d", viewportWidth, viewportHeight);
        
        //Vertex
        curlVertices = malloc(verticesSize);
        
        for (int y=0; y<yRes+1; ++y) {
            GLfloat tv = (GLfloat)y/yRes;
            GLfloat vy = tv * viewportHeight;
            for (int x=0; x<xRes+1; ++x) {
                Vertex4Curl *v = &curlVertices[y*(xRes+1) + x];
                v->u = (GLfloat)x/xRes;
                v->v = 1 - tv;
                v->x = v->u * viewportWidth;
                v->y = vy;
                v->z = 0;
            }
        }
        
        //Indices
        elementCount = xRes*yRes*2*3;
        GLsizeiptr indicesSize = elementCount*sizeof(GLushort);//Two triangles per square, 3 indices per triangle
        curlIndices = malloc(indicesSize);
        for (int y=0; y<yRes; ++y) {
            for (int x=0; x<xRes; ++x) {
                int i = y*(xRes+1) + x;
                int idx = y*xRes + x;
                assert(i < elementCount*3-1);
                curlIndices[idx*6+0] = i;
                curlIndices[idx*6+1] = i + 1;
                curlIndices[idx*6+2] = i + xRes + 1;
                curlIndices[idx*6+3] = i + 1;
                curlIndices[idx*6+4] = i + xRes + 2;
                curlIndices[idx*6+5] = i + xRes + 1;
            }
        }
        
        //init VideoFragment array
        NSMutableArray *tempVideoFragmentArray = [NSMutableArray array];
        for (int i = 0; i < kPhotoMAXNum; i++) {
            
            VideoFragment *videoFragment = [VideoFragment new];
            
            //init texure
            TextureInfo textureInfo;
            GLuint texName;
            glGenTextures(1, &texName);
            glBindTexture(GL_TEXTURE_2D, texName);
            textureInfo.textureId = texName;

            videoFragment.textureInfo = textureInfo;
            
            //init animaDetail
            AnimaDetail *animaDetail = [AnimaDetail new];
            
            videoFragment.animaDetail = animaDetail;
            
            [tempVideoFragmentArray addObject:videoFragment];
        }
        
        self.videoFragmentArray = tempVideoFragmentArray;
        
        //视频设置
        NSMutableArray *videoTempArray = [NSMutableArray array];
        for (int i = 0; i < kOverlayMAXNum; i++) {
            
            VideoOverlayFragment *videoOverlayFragment = [VideoOverlayFragment new];
            
            VideoFragment *videoFragment = [VideoFragment new];
            
            TextureInfo textureInfo;
            GLuint texName;
            glGenTextures(1, &texName);
            glBindTexture(GL_TEXTURE_2D, texName);
            
            textureInfo.textureId = texName;
            textureInfo.size = CGSizeMake(0, 0);
            
            videoFragment.textureInfo = textureInfo;
            
            AnimaDetail *animaDetail = [AnimaDetail new];
            videoFragment.animaDetail = animaDetail;
            
            videoOverlayFragment.videoFragment = videoFragment;
            
            [videoTempArray addObject:videoOverlayFragment];
        }
        
        self.overlayVideosArray = videoTempArray;
        
        [self setupVBOs];
        
        [self defaultImages];
        
        [self play];
    }
    
    return self;
}

- (void)defaultImages
{
    switch (_mvType) {
        case kMVTypeMemory:
        {
            kPhotoNum = 10;
        }
            break;
        case kMVTypeSport:
        {
            kPhotoNum = 12;
        }
            break;
        case kMVTypePhoto:
        {
            kPhotoNum = 8;
        }
            break;
        case KMVTypeCurlPage:
        {
            kPhotoNum = 4;
        }
            break;
        default:
            break;
    }
    
    //加载照片
    NSMutableArray *tempImageArray = [NSMutableArray array];
    if(_mvType == kMVTypeMemory) {
        for (int i = 0; i < kPhotoNum; i++) {
            int index = MAX(1, i);
            UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"memory%02d.jpg", index]];
            [tempImageArray addObject:image];
        }
    }else if (_mvType == kMVTypePhoto) {
        
        for (int i = 0; i < kPhotoNum; i++) {
            UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"photo%02d.jpg", i + 1]];

            [tempImageArray addObject:image];
        }
        
        UIImage *image4 = tempImageArray[3];
        CGFloat ratio = 4;
        CGFloat testHeight = 256;
        CGFloat testWidth = 256;
        UIImage *repeatImage = MTCreateRepeatImage(image4, CGSizeMake(testWidth, testHeight), CGSizeMake(testWidth, testHeight - 4));
        [tempImageArray insertObject:repeatImage atIndex:4];
        
        testHeight = 512;
        testWidth = 512;
        UIImage *image6 = tempImageArray[5];
        repeatImage = MTCreateRepeatImage(image6, CGSizeMake(testWidth, testHeight), CGSizeMake(testWidth - 4, testHeight));
        [tempImageArray insertObject:repeatImage atIndex:6];

    }else if (_mvType == kMVTypeSport) {
        
        for (int i = 0; i < kPhotoNum; i++) {
            
            UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"sport%02d.png", i + 1]];
            [tempImageArray addObject:image];
        }
        
        [tempImageArray insertObject:[UIImage imageNamed:@"watermark@2x.png"] atIndex:10];
    }else if (_mvType == KMVTypeCurlPage) {
        for (int i = 0; i < kPhotoNum; i++) {
            
            UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"curlPage%02d.png", i + 1]];
            [tempImageArray addObject:image];
        }
        
        //1
        UIImage *oneImage = tempImageArray[0];
        oneImage = MTCreateCurlPageImage(oneImage, CGRectMake(-80, 50, 220, 220), M_PI_4 / 4);
        [tempImageArray replaceObjectAtIndex:0 withObject:oneImage];
        
        //2
        UIImage *twoImage = tempImageArray[1];
        twoImage = MTCreateCurlPageImage(twoImage, CGRectMake(50, -40, 220, 220), -M_PI_4 / 5);
        [tempImageArray replaceObjectAtIndex:1 withObject:twoImage];
        
        //3
        UIImage *threeImage = tempImageArray[2];
        threeImage = MTCreateCurlPageImage(threeImage, CGRectMake(50, 50, 220, 220), M_PI_4 / 20);
        [tempImageArray replaceObjectAtIndex:2 withObject:threeImage];
        
    }
    
    self.imagesArray = tempImageArray;
}

- (void)play
{
    _currentTime = 0;
    
    [self setupMVTypeConfig];
    
    //[self setupWriter];
    //[self setupAudio];

    [self setupImages];
    [self setupVideos];

    [self playAudio];
    
    [self setupDisplayLink];
}

- (void)setupMVTypeConfig
{
    switch (_mvType) {
        case kMVTypeMemory:
        {
            kPhotoNum = 10;
            kVideoNameArray = @[@"memory.mov", @"memory.mov", @"End.mov"];
            kAudioSound = @"memorySound.mp3";
            videoSumDuartion = [VideoModel videoDuration4Memory:self.imagesArray.count];
        }
            break;
        case kMVTypeSport:
        {
            kPhotoNum = 12;
            kVideoNameArray = @[@"End.mov"];
            kAudioSound = @"sports.mp3";
            videoSumDuartion = [VideoModel videoDuration4Sport:self.imagesArray.count];
        }
            break;
        case kMVTypePhoto:
        {
            kPhotoNum = 10;
            
            kVideoNameArray = @[@"photoOverlay03.mov", @"photoOverlay01.mov",@"photoOverlay01.mov",@"photoOverlay01.mov", @"photoOverlay01.mov", @"photoOverlay01.mov", @"photoOverlay01.mov", @"photoOverlay01.mov", @"photoOverlay01.mov", @"End.mov"];
            kAudioSound = @"photo.mp3";
            videoSumDuartion = [VideoModel videoDuration4Photo:self.imagesArray.count];
        }
            break;
        case KMVTypeCurlPage:
        {
            kPhotoNum = 4;
            
            kVideoNameArray = @[];
            kAudioSound = @"photo.mp3";
            videoSumDuartion = [VideoModel videoDuration4CurlPage:self.imagesArray.count];

        }
            break;
        default:
            break;
    }
}
- (void)setupAudio
{
    NSURL *audioUrl = [NSURL fileURLWithPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:kAudioSound]];
    AVURLAsset *audioAsset = [[AVURLAsset alloc] initWithURL:audioUrl options:nil];
    NSMutableDictionary* audioReadSettings = [NSMutableDictionary dictionary];
    [audioReadSettings setValue:[NSNumber numberWithInt:kAudioFormatLinearPCM]
                         forKey:AVFormatIDKey];
    
    NSArray * audioTracks = [audioAsset tracksWithMediaType:AVMediaTypeAudio];
    readerOutput = [AVAssetReaderAudioMixOutput assetReaderAudioMixOutputWithAudioTracks:audioTracks audioSettings:audioReadSettings];
    
    audioReader = [[AVAssetReader alloc] initWithAsset:audioAsset error:nil];
    [audioReader addOutput:readerOutput];
    
    [audioReader startReading];
}

- (void)playAudio
{
    //audio
    NSURL *audioUrl = [NSURL fileURLWithPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:kAudioSound]];
    AVURLAsset *audioAsset = [[AVURLAsset alloc] initWithURL:audioUrl options:nil];
    
    AVMutableComposition* composition = [AVMutableComposition composition];
    
    AVMutableCompositionTrack *audioTrack1 = [composition addMutableTrackWithMediaType:AVMediaTypeAudio
                                                                      preferredTrackID:kCMPersistentTrackID_Invalid];
    
    [audioTrack1 insertTimeRange:CMTimeRangeMake(kCMTimeZero, audioAsset.duration)
                         ofTrack:[[audioAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0]
                          atTime:kCMTimeZero
                           error:nil];
    
    AVPlayerItem *playerItem = [AVPlayerItem playerItemWithAsset:composition];
    
    self.player = [AVPlayer playerWithPlayerItem:playerItem];
    
    [_player play];
}

- (void)setupVideos
{
    NSArray *videosFragmentArray;
    
    switch (_mvType) {
        case kMVTypeMemory:
        {
            videosFragmentArray = [VideoModel videoFragmentArray4Memory:videoSumDuartion];
        }
            break;
        case kMVTypeSport:
        {
            videosFragmentArray = [VideoModel videoFragmentArray4Sport:videoSumDuartion];
        }
            break;
        case kMVTypePhoto:
        {
            videosFragmentArray = [VideoModel videoFragmentArray4Photo:videoSumDuartion];
        }
            break;
        default:
            break;
    }
    
    for (int i = 0; i < videosFragmentArray.count; i++) {
        
        VideoOverlayFragment *videoOverlayFragment = self.overlayVideosArray[i];
        
        videoOverlayFragment.shaderParams = simpleShaderParams;
        
        NSDictionary *animaDetailDic = videosFragmentArray[i][kAnimaDetailKey];
        NSArray *animaFragmentArray = animaDetailDic[kAnimaFragmentKey];
        
        AnimaDetail *animaDetail = videoOverlayFragment.videoFragment.animaDetail;
        animaDetail.startTime = [animaDetailDic[kAnimationStartTimeKey] floatValue];
        animaDetail.endTime =  [animaDetailDic[kAnimationEndTimeKey] floatValue];
        NSMutableArray *animaFragments = [NSMutableArray array];
        
        for (int j = 0; j < animaFragmentArray.count; j++) {
            
            NSDictionary *animaFramentDic = animaFragmentArray[j];
            AnimaFragment *animaFragment = [AnimaFragment new];
            
            animaFragment.duration = [animaFramentDic[kAnimaFragmentDurationKey] floatValue];
            animaFragment.animationType = [animaFramentDic[kAnimationTypeKey] integerValue];
            animaFragment.filterType = [animaFramentDic[kFilterTypeKey] integerValue];
            animaFragment.shaderParams = simpleShaderParams;
            animaFragment.filterParam = animaFramentDic[kFitlerParamKey];
            
            NSArray *positonsArray = animaFramentDic[kPositonKey];
            
            NSMutableArray *positions = [NSMutableArray array];
            
            for (int k = 0; k < positonsArray.count; k++) {
                
                NSArray *positionXYZ = positonsArray[k];
                CC3Vector position = CC3VectorMake([positionXYZ[0] floatValue], [positionXYZ[1] floatValue], [positionXYZ[2] floatValue]);
                
                NSValue *positionValue = [NSValue valueWithBytes:&position objCType:@encode(CC3Vector)];
                [positions addObject:positionValue];
            }
            
            animaFragment.positions = positions;
            
            [animaFragments addObject:animaFragment];
        }
        
        animaDetail.animaFragments = animaFragments;
    }
    
    NSInteger index = -1;
    for (NSString *videoName in kVideoNameArray) {
        
        index++;

        VideoOverlayFragment *videoOverlayFragment = self.overlayVideosArray[index];
        
        NSString *file=[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:videoName];
        NSURL * url = [NSURL fileURLWithPath:file];
        AVURLAsset *mAsset = [[AVURLAsset alloc] initWithURL:url options:NULL];
        
        NSArray * tracks = [mAsset tracksWithMediaType:AVMediaTypeVideo];
        
        AVAssetTrack *mTrack = [tracks objectAtIndex:0];;
        NSLog(@"Tracks: %i", [tracks count]);
        
        NSString* key = (NSString*)kCVPixelBufferPixelFormatTypeKey;
        NSNumber* value = [NSNumber numberWithUnsignedInt:kCVPixelFormatType_32BGRA];
        NSDictionary * settings = [[NSDictionary alloc] initWithObjectsAndKeys:value, key, nil];
        
        AVAssetReaderTrackOutput *mOutput = [[AVAssetReaderTrackOutput alloc]
                        initWithTrack:mTrack outputSettings:settings];
        
        NSError *outError = nil;
        AVAssetReader *mReader = [[AVAssetReader alloc] initWithAsset:mAsset error:&outError];
        
        NSLog(@"outError:%@", outError.description);
        [mReader addOutput:mOutput];
        [mReader startReading];

        videoOverlayFragment.mOutput = mOutput;
        videoOverlayFragment.mReader = mReader;
        videoOverlayFragment.finished = NO;
    }
}

- (void)setupImages
{
    //animaDetail configure
    
    NSArray *originalArray;
    
    switch (_mvType) {
        case kMVTypeMemory:
        {
            originalArray = [VideoModel animaDetailConf4Memory:self.imagesArray.count];
        }
            break;
        case kMVTypeSport:
        {
            originalArray = [VideoModel animaDetailConf4Sport:self.imagesArray.count];
        }
            break;
        case kMVTypePhoto:
        {
            originalArray = [VideoModel animaDetailConf4Photo:self.imagesArray.count];
        }
            break;
        case KMVTypeCurlPage:
        {
            originalArray = [VideoModel animaDetailConf4CurlPage:self.imagesArray.count];
        }
            break;
        default:
            break;
    }
    
    NSMutableArray *tempArray = [NSMutableArray array];
    
    for (int i = 0; i < originalArray.count; i++) {
        
        VideoFragment *videoFragment = self.videoFragmentArray[i];
        videoFragment.imageRepeatParam = nil;
        
        NSDictionary *videoFragmentDic = originalArray[i];
        AnimaDetail *animaDetail = videoFragment.animaDetail;
        
        NSDictionary *animaDetailDic = videoFragmentDic[kAnimaDetailKey];
        videoFragment.photoIndex = [animaDetailDic[kAnimaDetailPhotoSort] integerValue];

        animaDetail.startTime = [animaDetailDic[kAnimationStartTimeKey] floatValue];
        animaDetail.endTime = [animaDetailDic[kAnimationEndTimeKey] floatValue];
        
        NSArray *animaFragmentArray = animaDetailDic[kAnimaFragmentKey];
        
        NSMutableArray *animaFragments = [NSMutableArray array];
        
        for (int j = 0; j < animaFragmentArray.count; j++) {
            
            NSDictionary *animaFramentDic = animaFragmentArray[j];
            AnimaFragment *animaFragment = [AnimaFragment new];
            
            animaFragment.duration = [animaFramentDic[kAnimaFragmentDurationKey] floatValue];
            animaFragment.animationType = [animaFramentDic[kAnimationTypeKey] integerValue];
            animaFragment.filterType = [animaFramentDic[kFilterTypeKey] integerValue];
            animaFragment.filterParam = animaFramentDic[kFitlerParamKey];
            animaFragment.imageRepeatParam = animaFramentDic[kImageRepeatKey];
            animaFragment.enlargeParam = animaFramentDic[kEnlargeKey];
            videoFragment.imageRepeatParam = animaFramentDic[kImageRepeatKey];
            animaFragment.animationParam = animaFramentDic[kAmimationParamKey];
            
            //滤镜参数
            if (animaFragment.filterType == kFilterTypeMotionBlur) {
                animaFragment.shaderParams = motionBlurShaderParmas;
                
            } else if (animaFragment.filterType == kFilterTypeExposure || animaFragment.filterType == kFilterTypeExposureLight) {
                animaFragment.shaderParams = exposureShaderParmas;
                
            } else if (animaFragment.filterType == kFilterTypeGrayscale) {
                animaFragment.shaderParams = grayscaleParmas;
                
            } else {
                animaFragment.shaderParams = simpleShaderParams;
            }

            //四边形，坐标参数
            if (animaFragment.animationType == kAnimationTypeQuadrangle) {
                
                animaFragment.quadParam = animaFramentDic[kQuadParamKey];
            }else if (animaFragment.animationType == kAnimationTypeSlide) {
                
                animaFragment.quadParam = animaFramentDic[kQuadParamKey];
            }else if (animaFragment.animationType == kAnimationTypeCurl) {
                
                animaFragment.shaderParams = frontVertexParmas;
                animaFragment.shaderParams2 = backVertexParmas;
            }else {
                
                animaFragment.quadParam = nil;
            }
            
            NSArray *positonsArray = animaFramentDic[kPositonKey];
            
            NSMutableArray *positions = [NSMutableArray array];
            
            for (int k = 0; k < positonsArray.count; k++) {
                
                NSArray *positionXYZ = positonsArray[k];
                CC3Vector position = CC3VectorMake([positionXYZ[0] floatValue], [positionXYZ[1] floatValue], [positionXYZ[2] floatValue]);
                
                NSValue *positionValue = [NSValue valueWithBytes:&position objCType:@encode(CC3Vector)];
                [positions addObject:positionValue];
            }
            
            animaFragment.positions = positions;
            
            [animaFragments addObject:animaFragment];
        }
        
        animaDetail.animaFragments = animaFragments;
        
        videoFragment.animaDetail = animaDetail;
        
        [tempArray addObject:videoFragment];
    }
    
    self.photosArray = tempArray;
    
    for (int i = 0; i < self.photosArray.count; i++) {
        
        VideoFragment *videoFragment = self.photosArray[i];
        
        UIImage *image = self.imagesArray[videoFragment.photoIndex];
        
        TextureInfo texureInfo = videoFragment.textureInfo;
        if (videoFragment.imageRepeatParam) {
            
            SetupTextureWithImage(image, &texureInfo, NO);
        }else {
            SetupTextureWithImage(image, &texureInfo, YES);
        }
        
        videoFragment.textureInfo = texureInfo;
    }
}

- (void)setupWriter
{
    movieWritingQueue = dispatch_queue_create("movieWritingQueue", NULL);
    fileType = AVFileTypeAppleM4V;
    
    NSString *MOVIE_PATH = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Movie.m4v"];
    unlink([MOVIE_PATH UTF8String]);
    movieURL = [NSURL fileURLWithPath:MOVIE_PATH];
    
    NSError *error = nil;
    
    assetWriter = [[AVAssetWriter alloc] initWithURL:movieURL fileType:AVFileTypeAppleM4V error:&error];
    if (error != nil)
    {
        NSLog(@"Error: %@", error);
    }
    
    
    NSMutableDictionary * outputSettings = [[NSMutableDictionary alloc] init];
    [outputSettings setObject: AVVideoCodecH264 forKey: AVVideoCodecKey];
    [outputSettings setObject: [NSNumber numberWithInt: viewportWidth * 2] forKey: AVVideoWidthKey];
    [outputSettings setObject: [NSNumber numberWithInt: viewportHeight * 2] forKey: AVVideoHeightKey];
    
    assetWriterVideoInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo outputSettings:outputSettings];
    assetWriterVideoInput.expectsMediaDataInRealTime = YES;
    
    // You need to use BGRA for the video in order to get realtime encoding. I use a color-swizzling shader to line up glReadPixels' normal RGBA output with the movie input's BGRA. kCVPixelFormatType_32RGBA
    NSDictionary *sourcePixelBufferAttributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithInt:kCVPixelFormatType_32BGRA], kCVPixelBufferPixelFormatTypeKey,
                                                           [NSNumber numberWithInt:viewportWidth], kCVPixelBufferWidthKey,
                                                           [NSNumber numberWithInt:viewportHeight], kCVPixelBufferHeightKey,
                                                           nil];
    
    assetWriterPixelBufferInput = [AVAssetWriterInputPixelBufferAdaptor assetWriterInputPixelBufferAdaptorWithAssetWriterInput:assetWriterVideoInput sourcePixelBufferAttributes:sourcePixelBufferAttributesDictionary];
    
    [assetWriter addInput:assetWriterVideoInput];
    
    AudioChannelLayout acl;
    bzero(&acl, sizeof(acl));
    acl.mChannelLayoutTag = kAudioChannelLayoutTag_Stereo;
    
    NSDictionary*  audioOutputSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [ NSNumber numberWithInt: kAudioFormatMPEG4AAC], AVFormatIDKey,
                                          [ NSNumber numberWithInt: 2 ], AVNumberOfChannelsKey,
                                          [ NSNumber numberWithFloat: 44100.0 ], AVSampleRateKey,
                                          [ NSData dataWithBytes: &acl length: sizeof( AudioChannelLayout ) ], AVChannelLayoutKey,
                                          [ NSNumber numberWithInt: 64000 ], AVEncoderBitRateKey,
                                          nil];
    
    assetWriterAudioInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeAudio outputSettings:audioOutputSettings];
    assetWriterAudioInput.expectsMediaDataInRealTime = YES;
    
    [assetWriter addInput:assetWriterAudioInput];
    
    dispatch_sync(movieWritingQueue, ^{
        [assetWriter startWriting];
        [assetWriter startSessionAtSourceTime:kCMTimeZero];
    });
}

- (void)createDataFBO
{
    glActiveTexture(GL_TEXTURE2);
    glGenFramebuffers(1, &movieFramebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, movieFramebuffer);

    glGenRenderbuffers(1, &movieRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, movieRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA8_OES, (int)viewportWidth, (int)viewportHeight);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, movieRenderbuffer);
}

- (void)save2Album
{

    dispatch_sync(movieWritingQueue, ^{
        NSLog(@"assetWriter_status:%i", assetWriter.status);
        [assetWriterVideoInput markAsFinished];
        [assetWriter finishWritingWithCompletionHandler:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
                
                [library writeVideoAtPathToSavedPhotosAlbum:movieURL
                                            completionBlock:^(NSURL *assetURL, NSError *error) {
                                                if (error) {
                                                    NSLog(@"%@: Error saving context: %@", [self class], [error localizedDescription]);
                                                }
                                            }];
            });
        }];
        
    });
}

- (void)dealloc
{
    _context = nil;
}

#pragma mark - public
- (void)playDemoWithType:(MVType)mvType
{
    [_displayLink invalidate];
    [_player pause];
 
    _mvType = mvType;
    [self defaultImages];
    [self play];
}

- (void)stopPlay
{
    [_displayLink invalidate];
    [_player pause];
}

- (void)startPlay
{
    [_displayLink invalidate];
    [_player pause];
    [self play];
}

- (void)setDisplayImages:(NSArray *)imageArray
{
    NSMutableArray *tempArray = [NSMutableArray arrayWithArray:imageArray];
    if(_mvType == kMVTypeMemory) {
        [tempArray insertObject:imageArray[0] atIndex:0];
    }else if (_mvType == kMVTypePhoto) {
        
        UIImage *image4 = tempArray[3];
        CGFloat ratio = 4;
        CGFloat testHeight = 256;
        CGFloat testWidth = 256;
        UIImage *repeatImage = MTCreateRepeatImage(image4, CGSizeMake(testWidth, testHeight), CGSizeMake(testWidth, testHeight));
        [tempArray insertObject:repeatImage atIndex:4];
        
        testHeight = 512;
        testWidth = 512;
        UIImage *image6 = tempArray[5];
        repeatImage = MTCreateRepeatImage(image6, CGSizeMake(testWidth, testHeight), CGSizeMake(testWidth - 5, testHeight));
        [tempArray insertObject:repeatImage atIndex:6];
    }else if (_mvType == kMVTypeSport) {
        [tempArray insertObject:[UIImage imageNamed:@"watermark@2x.png"] atIndex:10];
    }
    
    self.imagesArray = tempArray;
    
    NSLog(@"imagesArray:%@", self.imagesArray);
}

#pragma mark - render
- (void)render2:(CADisplayLink*)displayLink
{
    if (_currentTime > videoSumDuartion + 1) {
        [displayLink invalidate];
        [_player pause];
        [self play];
        //[self save2Album];
        return;
    }
    
    if ([self.player respondsToSelector:@selector(setVolume:)]) {
        if (_currentTime > videoSumDuartion - 2.5) {
            CGFloat ratio = (videoSumDuartion + 1 - _currentTime) / 3.5;
            self.player.volume = ratio;
        }else {
            self.player.volume = 1;
        }
    }else {
        
        if (_currentTime > videoSumDuartion - 2.5) {
            [self.player pause];
        }
    }
    
    [self render:displayLink];
    
    _currentTime += displayLink.duration * 2;
    
#if 0
    //Record
    static NSInteger frame = 0;
    CVPixelBufferRef pixel_buffer = NULL;
    
    CVReturn status = CVPixelBufferPoolCreatePixelBuffer (NULL, [assetWriterPixelBufferInput pixelBufferPool], &pixel_buffer);
    
    if ((pixel_buffer == NULL) || (status != kCVReturnSuccess))
    {
        NSLog(@"pixel_buffer_is_nil:%i", status);
        CVPixelBufferRelease(pixel_buffer);
        return;
    }
    
    CVPixelBufferLockBaseAddress(pixel_buffer, 0);
    
    GLubyte *pixelBufferData = (GLubyte *)CVPixelBufferGetBaseAddress(pixel_buffer);
    const int w = self.frame.size.width;
    const int h = self.frame.size.height;
    int s = 1;
    const NSInteger myDataLength = w * h * 4 * s * s;
    
    GLubyte *buffer2 = (GLubyte *) malloc(myDataLength);
    glReadPixels(0, 0, videoSize.width, videoSize.height, GL_RGBA, GL_UNSIGNED_BYTE, buffer2);
    
    
    dispatch_async(movieWritingQueue, ^{
        /*************/
        
        GLubyte *buffer3 = (GLubyte *) malloc(myDataLength);
        
        for (int y = 0; y < h; y++) {
            for (int x = 0; x < w; x++) {
                const unsigned char* pixel_in = &buffer2[(y * w + x) * 4];
                unsigned char* pixel_out = &buffer3[(y * w + x) * 4];
                pixel_out[0] = pixel_in[2];
                pixel_out[1] = pixel_in[1];
                pixel_out[2] = pixel_in[0];
                pixel_out[3] = pixel_in[3];
            }
        }
        
        free(buffer2);
        
        for(int y = 0; y < videoSize.width; y++)
        {
            memcpy(pixelBufferData + (h*s - 1 - y) * w * 4 * s, buffer3 + (y * 4 * w * s), w * 4 * s );
        }
        
        
        free(buffer3);
        /*************/
        
        while( ! assetWriterVideoInput.readyForMoreMediaData) {
            NSDate *maxDate = [NSDate dateWithTimeIntervalSinceNow:0.1];
            //            NSLog(@"video waiting...");
            [[NSRunLoop currentRunLoop] runUntilDate:maxDate];
        }
        
        CMTime frameTime = CMTimeMake(frame,30);
        if (!assetWriterVideoInput.readyForMoreMediaData)
        {
        }
        else if(![assetWriterPixelBufferInput appendPixelBuffer:pixel_buffer withPresentationTime:frameTime])
        {
            //NSLog(@"Problem appending pixel buffer at time: %@", CFBridgingRelease(CMTimeCopyDescription(kCFAllocatorDefault, frameTime)));
        }
        else
        {
            //NSLog(@"Wrote a video frame: %@", CFBridgingRelease(CMTimeCopyDescription(kCFAllocatorDefault, frameTime)));
        }
        
        if (audioReader) {
            if ( AVAssetReaderStatusCompleted == audioReader.status) {
                
            }else {
                CMSampleBufferRef audioAampleBuffer = [readerOutput copyNextSampleBuffer];
                
                if (audioAampleBuffer == NULL) {
                    NSLog(@"audioAampleBuffer_NULL");
                    
                }else {
                    if( ![assetWriterAudioInput appendSampleBuffer:audioAampleBuffer] )
                    {
                        NSLog(@"assetWriterAudioInput_appendSampleBuffer_failed");
                    }
                }
            }
        }
        
        CVPixelBufferUnlockBaseAddress(pixel_buffer, 0);
        frame++;
        CVPixelBufferRelease(pixel_buffer);
        
    });
#endif
}

- (void)render:(CADisplayLink*)displayLink {
    
    glEnable(GL_BLEND);
    glDisable(GL_CULL_FACE);
    //glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    
    glClearColor(0, 0, 0, 1);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    
    glViewport(0, 0, viewportWidth, viewportHeight);
    
    //images
    NSInteger index = -1;
    
    for (VideoFragment *videoFragment in _photosArray) {
        
        glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
        
        index++;
        
        //不在自己的时间段就不显示
        CFTimeInterval currentTime = _currentTime;
        if (currentTime < videoFragment.animaDetail.startTime) {
            continue;
        }else if ((currentTime > videoFragment.animaDetail.endTime) && (Endless != videoFragment.animaDetail.endTime)) {
            continue;
        }else {
            //Go on
        }
        
        AnimaDetail *animaDetail = videoFragment.animaDetail;
        
        //根据图片宽高比，计算图片显示图片区域
        float screenAspectRatio = viewportWidth / viewportHeight;
        float photoRatio = (videoFragment.textureInfo.size.width / videoFragment.textureInfo.size.height);
        float ratio = photoRatio / screenAspectRatio;
        float hZoomValue = MIN(1 / ratio, 1); //ratio > 1?(1 / ratio): 1;
        float vZoomValue = MIN(ratio, 1); //ratio > 1? 1: ratio;
        float z = -4.0;
        float zoom = 2.0 / 4.0;
        
        CC3GLMatrix *modelView = [CC3GLMatrix matrix];
        CGFloat alpha = 1;
        
        //显示方图，多余的不显示
        Vertex vertices[] ={{{ 1,  -1, 0}, {(1 + hZoomValue) / 2, (1 + vZoomValue) / 2}},
            {{ 1,   1, 0}, {(1 + hZoomValue) / 2, (1 - vZoomValue) / 2}},
            {{-1,   1, 0}, {(1 - hZoomValue) / 2, (1 - vZoomValue) / 2}},
            {{-1,  -1, 0}, {(1 - hZoomValue) / 2, (1 + vZoomValue) / 2}},
        };
        
        //计算图片处在哪一个动画片段内
        float time4calc = animaDetail.startTime;
        
        NSArray *animaFragments = animaDetail.animaFragments;
        
        AnimaFragment *curAnimaFragment;
        
        for (int i = 0; i < animaFragments.count; i++) {
            
            AnimaFragment *animaFragment = animaDetail.animaFragments[i];
            if (currentTime > animaFragment.duration + time4calc) {
                time4calc += animaFragment.duration;
            }else {
                curAnimaFragment = animaFragment;
                break;
            }
        }
        
        //set up projection
        ShaderParams shaderParams = curAnimaFragment.shaderParams;
        glUseProgram(shaderParams.programHandle);
        CC3GLMatrix *projection = [CC3GLMatrix matrix];
        [projection populateFromFrustumLeft:-1. * zoom andRight:1. * zoom andBottom:-1 * zoom andTop:1 * zoom andNear:2 andFar:7];
        
        glUniformMatrix4fv(shaderParams.uniforms[UNIFORM_PROJECTION], 1, 0, projection.glMatrix);
        
        //计算当前坐标
        CC3Vector curPosition;
        CGFloat ratioPosition = 0;
        
        float elapsedTime = currentTime - time4calc;
        float durationTime = curAnimaFragment.duration;
        
        NSInteger positionsCount = curAnimaFragment.positions.count;
        float durationRatio = elapsedTime/durationTime;
        
        if (positionsCount == 1) {
            
            NSValue *positionValue = curAnimaFragment.positions[0];
            [positionValue getValue:&curPosition];
        }else if (positionsCount == 2) {
            
            CC3Vector prePosition;
            CC3Vector folPosition;
            
            NSValue *positionValue = curAnimaFragment.positions[0];
            [positionValue getValue:&prePosition];
            
            positionValue = curAnimaFragment.positions[1];
            [positionValue getValue:&folPosition];
            
            CGFloat ratioPosition = durationRatio;
            //NSLog(@"ratioPosition:%f", ratioPosition);
            curPosition = CC3VectorMake((prePosition.x * (1 - ratioPosition) + folPosition.x * ratioPosition), (prePosition.y * (1 - ratioPosition) + folPosition.y * ratioPosition), (prePosition.z * (1 - ratioPosition) + folPosition.z * ratioPosition));
        }else {
            
            float intervalPerPart = curAnimaFragment.duration / (positionsCount - 1);
            int preIndex4Position = MIN(floor(elapsedTime / intervalPerPart), positionsCount - 2);
            
            ratioPosition = (elapsedTime - intervalPerPart * preIndex4Position) / intervalPerPart;
            
            CC3Vector prePosition;
            CC3Vector folPosition;
            
            NSValue *positionValue = curAnimaFragment.positions[preIndex4Position];
            [positionValue getValue:&prePosition];
            positionValue = curAnimaFragment.positions[preIndex4Position + 1];
            [positionValue getValue:&folPosition];
            
            curPosition = CC3VectorMake((prePosition.x * (1 - ratioPosition) + folPosition.x * ratioPosition), (prePosition.y * (1 - ratioPosition) + folPosition.y * ratioPosition), (prePosition.z * (1 - ratioPosition) + folPosition.z * ratioPosition));
        }
        
        //animationTypes
        if (curAnimaFragment.animationType == kAnimationTypeExpand) {
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            CGFloat expandRatio = MIN(1, elapsedTime/curAnimaFragment.duration + 0.2);
            
            float percent[2] = {expandRatio, expandRatio};
            Vertices4Expand(vertices, percent);
            
            alpha = elapsedTime/curAnimaFragment.duration;
            alpha = alpha * alpha * alpha;
        }else if (curAnimaFragment.animationType == kAnimationTypeQuadrangle) {
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            
            NSArray *quadParamArray = curAnimaFragment.quadParam;
            if (quadParamArray.count > 0) {
                
                CC3Vector verctors[quadParamArray.count];
                for (int i = 0; i < quadParamArray.count; i++) {
                    NSArray *verctorArray = quadParamArray[i];
                    CC3Vector *verctor = &verctors[i];
                    *verctor = CC3VectorMake([verctorArray[0] floatValue], [verctorArray[1] floatValue], [verctorArray[2] floatValue]);
                }
                
                Vertices4CustomQuad(vertices, verctors);
            }
            //alpha = interval/curAnimaFragment.duration;
            //alpha = alpha * alpha * alpha;
        }else if (curAnimaFragment.animationType == kAnimationTypeSlide) {
            //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            
            NSArray *quadParamArray = curAnimaFragment.quadParam;
            if (quadParamArray.count > 0) {
                
                CC3Vector verctors[quadParamArray.count / 2];
                for (int i = 0; i < quadParamArray.count / 2; i++) {
                    NSArray *verctorArray1 = quadParamArray[i];
                    NSArray *verctorArray2 = quadParamArray[i + quadParamArray.count / 2];
                    float x,y;
                    
                    x = [verctorArray1[0] floatValue] * (1 -durationRatio) + [verctorArray2[0] floatValue] * durationRatio;
                    y = [verctorArray1[1] floatValue] * (1 -durationRatio) + [verctorArray2[1] floatValue] * durationRatio;
                    
                    CC3Vector *verctor = &verctors[i];
                    *verctor = CC3VectorMake(x, y, 0);
                }
                
                Vertices4CustomQuad(vertices, verctors);
            }
        }else if (curAnimaFragment.animationType == kAnimationTypeCurl) {
            glEnable(GL_CULL_FACE);

            GLuint xRes = viewportWidth / 10;
            GLuint yRes = viewportHeight / 10;
            GLsizeiptr verticesSize = (xRes+1)*(yRes+1)*sizeof(Vertex4Curl);
            GLsizeiptr indicesSize = elementCount*sizeof(GLushort);
            
            GLuint vertexBuffer = videoFragment.vertexBuffer;
            glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
            glBufferData(GL_ARRAY_BUFFER, verticesSize, (GLvoid *)curlVertices, GL_STATIC_DRAW);
            videoFragment.vertexBuffer = vertexBuffer;
            
            GLuint indexBuffer = videoFragment.indexBuffer;
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesSize, (GLvoid *)curlIndices, GL_STATIC_DRAW);
            videoFragment.indexBuffer = indexBuffer;
        }
        
        if (curAnimaFragment.enlargeParam) {
            
            float ratio = 1;
            CGPoint position = CGPointMake(0.5, 0.5);
            
            NSArray *tempArray = curAnimaFragment.enlargeParam;
            if (tempArray.count > 1) {
                NSArray *subTempArray1 = tempArray[0];
                NSArray *subTempArray2 = tempArray[1];
                
                ratio = [subTempArray1[0] floatValue] * (1 -durationRatio) + [subTempArray2[0] floatValue] * durationRatio;
                CGFloat x = [subTempArray1[1] floatValue] * (1 -durationRatio) + [subTempArray2[1] floatValue] * durationRatio;
                CGFloat y = [subTempArray1[2] floatValue] * (1 -durationRatio) + [subTempArray2[2] floatValue] * durationRatio;
                position = CGPointMake(x, y);
            }else {
                
                NSArray *subTempArray1 = tempArray[0];
                ratio = [subTempArray1[0] floatValue];
                position = CGPointMake([subTempArray1[1] floatValue], [subTempArray1[2] floatValue]);
            }
            
            if (ratio > 1) {
                Vertices4Enlarge(vertices, ratio, position);
            }
        }
        
        if (videoFragment.imageRepeatParam) {
            
            NSArray *imageRepeatArray = videoFragment.imageRepeatParam;
            
            float ratio[2] = {[imageRepeatArray[0] floatValue], [imageRepeatArray[1] floatValue]};
            Vertices4Repeat(vertices, ratio);
        }
        
        [modelView populateFromTranslation:CC3VectorMake(curPosition.x, curPosition.y, z + curPosition.z)];
        glUniformMatrix4fv(shaderParams.uniforms[UNIFORM_MODELVIEW], 1, 0, modelView.glMatrix);
        glActiveTexture(GL_TEXTURE0); // unneccc in practice
        glBindTexture(GL_TEXTURE_2D, videoFragment.textureInfo.textureId);
        glUniform1i(shaderParams.uniforms[UNIFORM_TEXTURE], 0); // unnecc in practice
        
        glActiveTexture(GL_TEXTURE1); // unneccc in practice
        glBindTexture(GL_TEXTURE_2D, curAnimaFragment.filterTextureInfo.textureId);
        glUniform1i(shaderParams.uniforms[UNIFORM_TEXTURE1], 1); // unnecc in practice
        
        if (curAnimaFragment.filterType == kFilterTypeMotionBlur) {
            NSArray *filterParam = curAnimaFragment.filterParam;
            
            GLfloat positionArray[2];
            positionArray[0] = [filterParam[0] floatValue] / videoFragment.textureInfo.size.width;
            positionArray[1] = [filterParam[1] floatValue] / videoFragment.textureInfo.size.height;
            
            glUniform2fv(shaderParams.uniforms[UNIFORM_MOTION_BLUR_STEP], 1, positionArray);
        }else if (curAnimaFragment.filterType == kFilterTypeExposure) {
            glUniform1f(shaderParams.uniforms[UNIFORM_EXPOSURE],  - 2 * durationRatio);
        }else if (curAnimaFragment.filterType == kFilterTypeExposureLight) {
            glUniform1f(shaderParams.uniforms[UNIFORM_EXPOSURE],  1 - 1 * 4 * (durationRatio - 0.5) * (durationRatio - 0.5));
            [modelView populateFromTranslation:CC3VectorMake(curPosition.x, curPosition.y, z + curPosition.z + 0.1 * (1 - 4 * (durationRatio - 0.5) * (durationRatio - 0.5)))];
            
            glUniformMatrix4fv(shaderParams.uniforms[UNIFORM_MODELVIEW], 1, 0, modelView.glMatrix);
        }else if (curAnimaFragment.filterType == kFilterTypeChangeAlpha) {
            
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            
            NSArray *filterParam = curAnimaFragment.filterParam;
            alpha = [filterParam[0] floatValue] * (1 -durationRatio) + [filterParam[1] floatValue] * durationRatio;
        }else if (curAnimaFragment.filterType == kFilterTypeGrayscale) {
            
            NSArray *filterParam = curAnimaFragment.filterParam;
            CGFloat saturation = [filterParam[0] floatValue] * (1 -durationRatio) + [filterParam[1] floatValue] * durationRatio;
            
            glUniform1f(shaderParams.uniforms[UNIFORM_SATURATION], saturation);
        }

        glUniform1f(shaderParams.uniforms[UNIFORM_ALPHA], alpha);
        
        if (curAnimaFragment.animationType == kAnimationTypeCurl) {
            
            glCullFace(GL_BACK);
            
            //projection
            //[projection populateFromFrustumLeft:-viewportWidth / 2. andRight:viewportWidth / 2 andBottom:-viewportHeight / 2 andTop:viewportHeight / 2 andNear:2 andFar:100];
            [projection populateOrthoFromFrustumLeft:-viewportWidth / 2. andRight:viewportWidth / 2 andBottom:-viewportHeight / 2 andTop:viewportHeight / 2 andNear:-100 andFar:100];
            glUniformMatrix4fv(shaderParams.uniforms[UNIFORM_PROJECTION], 1, GL_FALSE, projection.glMatrix);
            //modelView
            modelView = [CC3GLMatrix matrix];
            [modelView populateFromTranslation:CC3VectorMake( -viewportWidth / 2, -viewportHeight / 2, 0)];
            glUniformMatrix4fv(shaderParams.uniforms[UNIFORM_MODELVIEW], 1, GL_FALSE, modelView.glMatrix);
            
            //
            NSArray *paramArray = (NSArray *)curAnimaFragment.animationParam;
            if (!paramArray) {
                paramArray = @[@[@(viewportWidth),@(viewportHeight)],@(M_PI_2),@(32)];
            }
            NSArray *postionArray = paramArray[0];
            CGPoint cylinderPosition = CGPointMake([postionArray[0] floatValue], [postionArray[1] floatValue]);
            
            CGPoint p0 = self.cylinderPosition;
            CGFloat t = durationRatio * durationRatio;
            p0 = CGPointMake((1 - t)*p0.x + t*cylinderPosition.x, (1 - t)*p0.y + t*cylinderPosition.y);
            
            double a0 = self.cylinderAngle;
            double a1 = [paramArray[1] floatValue];
            a0 = (1 - t)*a0 + t*a1;
            
            CGFloat r = self.cylinderRadius;
            r = (1 - t)*r + t * [paramArray[2] floatValue];;
            
            CGPoint glCylinderPosition = CGPointMake(p0.x, (viewportHeight - p0.y));
            CGFloat glCylinderAngle = M_PI - a0;
            CGFloat glCylinderRadius = r;
            
            glUniform2f(shaderParams.uniforms[UNIFORM_CYLINDERPOSITION], glCylinderPosition.x, glCylinderPosition.y);
            glUniform2f(shaderParams.uniforms[UNIFORM_CYLINDERDIRECTION], cosf(glCylinderAngle), sinf(glCylinderAngle));
            glUniform1f(shaderParams.uniforms[UNIFORM_CYLINDERRADIUS], glCylinderRadius);
            
            glBindBuffer(GL_ARRAY_BUFFER, videoFragment.vertexBuffer);
            glEnableVertexAttribArray(shaderParams.attribs[ATTRIBS_VERTEX]);
            glVertexAttribPointer(shaderParams.attribs[ATTRIBS_VERTEX], 3, GL_FLOAT, GL_FALSE, sizeof(Vertex4Curl), (void *)offsetof(Vertex4Curl, x));
            glEnableVertexAttribArray(shaderParams.attribs[ATTRIBS_TEXCOORD]);
            glVertexAttribPointer(shaderParams.attribs[ATTRIBS_TEXCOORD], 2, GL_FLOAT, GL_FALSE, sizeof(Vertex4Curl), (void *)offsetof(Vertex4Curl, u));
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, videoFragment.indexBuffer);
            glDrawElements(GL_TRIANGLES, elementCount, GL_UNSIGNED_SHORT, (void *)0);
            
            //back
            glCullFace(GL_FRONT);
            ShaderParams shaderParams2 = curAnimaFragment.shaderParams2;
            glUseProgram(shaderParams2.programHandle);
            
            glActiveTexture(GL_TEXTURE0); // unneccc in practice
            glBindTexture(GL_TEXTURE_2D, videoFragment.textureInfo.textureId);
            glUniform1i(shaderParams2.uniforms[UNIFORM_TEXTURE], 0); // unnecc in practice
            
            glUniformMatrix4fv(shaderParams2.uniforms[UNIFORM_PROJECTION], 1, GL_FALSE, projection.glMatrix);
            glUniformMatrix4fv(shaderParams2.uniforms[UNIFORM_MODELVIEW], 1, GL_FALSE, modelView.glMatrix);
            
            glUniform2f(shaderParams2.uniforms[UNIFORM_CYLINDERPOSITION], glCylinderPosition.x, glCylinderPosition.y);
            glUniform2f(shaderParams2.uniforms[UNIFORM_CYLINDERDIRECTION], cosf(glCylinderAngle), sinf(glCylinderAngle));
            glUniform1f(shaderParams2.uniforms[UNIFORM_CYLINDERRADIUS], glCylinderRadius);
            
            glActiveTexture(GL_TEXTURE1); // unneccc in practice
            glBindTexture(GL_TEXTURE_2D, backTextureInfo.textureId);
            glUniform1i(shaderParams2.uniforms[UNIFORM_BACKGRADIENT], 1);
            
            glDrawElements(GL_TRIANGLES, elementCount, GL_UNSIGNED_SHORT, (void *)0);
        }else {
            
            glBindBuffer(GL_ARRAY_BUFFER, videoFragment.vertexBuffer);
            glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, videoFragment.indexBuffer);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Indices), Indices, GL_STATIC_DRAW);
            glVertexAttribPointer(shaderParams.attribs[ATTRIBS_TEXCOORD], 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) (sizeof(float) * 3));
            glVertexAttribPointer(shaderParams.attribs[ATTRIBS_VERTEX], 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
            glDrawElements(GL_TRIANGLE_STRIP, sizeof(Indices)/sizeof(Indices[0]), GL_UNSIGNED_BYTE, 0);
        }
    }
    
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    
    index = -1;
    for (int i = 0; i < kVideoNameArray.count; i ++) {
        index++;
        VideoOverlayFragment *videoOverlayFragment = self.overlayVideosArray[i];
        VideoFragment *overlayFragment = videoOverlayFragment.videoFragment;
        
        //overlay
        
        //不在自己的时间段就不显示
        CFTimeInterval currentTime = _currentTime;
        if (currentTime < overlayFragment.animaDetail.startTime) {
            continue;
        }else if ((currentTime > overlayFragment.animaDetail.endTime) && (Endless != overlayFragment.animaDetail.endTime)) {
            continue;
        }else {
            //Go on
        }
        
        AVAssetReaderTrackOutput *mOutput = videoOverlayFragment.mOutput;
        AVAssetReader *mReader = videoOverlayFragment.mReader;
        
        if (!videoOverlayFragment.finished) {
            sampleBuffer = [mOutput copyNextSampleBuffer];
        }
        
        if (AVAssetReaderStatusCompleted == mReader.status) {
            //NSLog(@"AVAssetReaderStatusCompleted");
            videoOverlayFragment.finished = YES;
        }else if (sampleBuffer == NULL) {
            NSLog(@"sampleBuffer == NULL");
            videoOverlayFragment.finished = YES;
        }
        
        if (!videoOverlayFragment.finished) {
            CVImageBufferRef pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
            CVPixelBufferLockBaseAddress( pixelBuffer, 0 );
            
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, overlayFragment.textureInfo.textureId);
            size_t width = CVPixelBufferGetWidth(pixelBuffer);
            size_t height = CVPixelBufferGetHeight(pixelBuffer);
            TextureInfo textureInfo;
            textureInfo.textureId = overlayFragment.textureInfo.textureId;
            textureInfo.size = CGSizeMake(width, height);
            overlayFragment.textureInfo  = textureInfo;
            
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (GLsizei)width, (GLsizei)height, 0, GL_BGRA_EXT, GL_UNSIGNED_BYTE, CVPixelBufferGetBaseAddress(pixelBuffer));
            
            CVPixelBufferUnlockBaseAddress( pixelBuffer, 0 );
            CFRelease(sampleBuffer);
            
            AnimaDetail *animaDetail = overlayFragment.animaDetail;
            AnimaFragment *animaFrament = animaDetail.animaFragments[0];
            ShaderParams overlayshaderParams = animaFrament.shaderParams;
            
            glUseProgram(overlayshaderParams.programHandle);
            glBindBuffer(GL_ARRAY_BUFFER, overlayFragment.vertexBuffer);
            
            CC3GLMatrix *projection = [CC3GLMatrix matrix];
            
            float screenAspectRatio = viewportWidth / viewportHeight;
            float photoRatio = (overlayFragment.textureInfo.size.width / overlayFragment.textureInfo.size.height);
            float ratio = photoRatio / screenAspectRatio;
            float hZoomValue = MIN(1 / ratio, 1); //ratio > 1?(1 / ratio): 1;
            float vZoomValue = MIN(ratio, 1); //ratio > 1? 1: ratio;
            float zoom = 1;
            
            [projection populateFromFrustumLeft:-1. * zoom andRight:1. * zoom andBottom:-1 * zoom andTop:1 * zoom andNear:2 andFar:7];
            glUniformMatrix4fv(overlayshaderParams.uniforms[UNIFORM_PROJECTION], 1, 0, projection.glMatrix);
            
            //显示方图，多余的不显示
            Vertex vertices[] ={{{ 1,  -1, 0}, {(1 + hZoomValue) / 2, (1 + vZoomValue) / 2}},
                {{ 1,   1, 0}, {(1 + hZoomValue) / 2, (1 - vZoomValue) / 2}},
                {{-1,   1, 0}, {(1 - hZoomValue) / 2, (1 - vZoomValue) / 2}},
                {{-1,  -1, 0}, {(1 - hZoomValue) / 2, (1 + vZoomValue) / 2}},
            };
            
            CC3GLMatrix *modelView = [CC3GLMatrix matrix];
            [modelView populateFromTranslation:CC3VectorMake(0, 0, -2)];
            glUniformMatrix4fv(overlayshaderParams.uniforms[UNIFORM_MODELVIEW], 1, 0, modelView.glMatrix);
            
            glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, overlayFragment.indexBuffer);
            
            glActiveTexture(GL_TEXTURE0); // unneccc in practice
            glBindTexture(GL_TEXTURE_2D, overlayFragment.textureInfo.textureId);
            glUniform1i(overlayshaderParams.uniforms[UNIFORM_TEXTURE], 0); // unnecc in practice
            
            CGFloat alpha = 0.2;
            if (animaFrament.filterParam && [animaFrament.filterParam isKindOfClass:[NSArray class]]) {
                NSArray *fitlerParamArray = animaFrament.filterParam;
                alpha = [fitlerParamArray[0] floatValue];
            }
            
            glUniform1f(overlayshaderParams.uniforms[UNIFORM_ALPHA], alpha);
            
            glVertexAttribPointer(overlayshaderParams.attribs[ATTRIBS_VERTEX], 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
            glVertexAttribPointer(overlayshaderParams.attribs[ATTRIBS_TEXCOORD], 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) (sizeof(float) * 3));
            
            glDrawElements(GL_TRIANGLE_STRIP, sizeof(Indices)/sizeof(Indices[0]), GL_UNSIGNED_BYTE, 0);
        }
    }
    
    /* If antialiasing is enabled, draw on the multisampling buffers */
    if (self.antialiasing) {
        
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER_APPLE, _framebuffer);
        glBindFramebuffer(GL_READ_FRAMEBUFFER_APPLE, _sampleFramebuffer);
        glResolveMultisampleFramebufferAPPLE();
        
        GLenum attachments[] = {GL_COLOR_ATTACHMENT0};
        glDiscardFramebufferEXT(GL_READ_FRAMEBUFFER_APPLE, 1, attachments);
        
        glBindFramebuffer(GL_FRAMEBUFFER, _sampleFramebuffer);
    }
    
    //Present
    [_context presentRenderbuffer:GL_RENDERBUFFER];
}

@end
